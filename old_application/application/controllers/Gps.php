<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gps extends CI_Controller
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		$this->load->model("gps_model");
	}

	public function index()
	{
		//echo 'GPS';
	}

	public function addData()
	{
		if ($this->input->get_request_header('app-token', TRUE) == $this->config->item('app_token')) {
			/*
			api.mobitrackbd.com/gps/addData
			{"imei":"AEIOU-12345","lat":"23.750538","lng":"90.367716","status":"123","speed":"50","course":"2","device_time":"2018-12-9 03:00:00"}
			*/

			$data = json_decode(file_get_contents('php://input'), true);

			if (isset($data) && $data != NULL) {
				// imei
				if (isset($data['imei'])) {
					$_POST['imei'] = $data['imei'];
				}
				// Lat
				if (isset($data['lat'])) {
					$_POST['lat'] = $data['lat'];
				}
				// lng
				if (isset($data['lng'])) {
					$_POST['lng'] = $data['lng'];
				}
				// Status
				if (isset($data['status'])) {
					$_POST['status'] = $data['status'];
				}
				// speed
				if (isset($data['speed'])) {
					$_POST['speed'] = $data['speed'];
				}
				// Course
				if (isset($data['course'])) {
					$_POST['course'] = $data['course'];
				}
				// device_time
				if (isset($data['device_time'])) {
					$_POST['device_time'] = $data['device_time'];
				}

				//print_r($data);

				// validation security 
				$this->form_validation->set_rules("imei", "1st", "required");
				$this->form_validation->set_rules("lat", "2nd", "required");
				$this->form_validation->set_rules("lng", "3rd", "required");
				$this->form_validation->set_rules("speed", "4th", "required");
				$this->form_validation->set_rules("status", "5th", "required");
				$this->form_validation->set_rules("course", "6th", "trim");
				$this->form_validation->set_rules("device_time", "7th", "trim");
				if ($this->form_validation->run() == FALSE) {
					// show error
					echo form_error("imei") . '';
					echo form_error("lat") . '';
					echo form_error("lng") . '';
					echo form_error("speed") . '';
					echo form_error("status") . '';
					echo form_error("course") . '';
					echo form_error("device_time") . '';
				} else {
					$this->gps_model->addData();
				}
			}
		} else {
			echo 'Header Failed.';
			return false;
		}
	}
}
