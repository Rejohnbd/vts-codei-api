<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 
		$this->load->model('customer_model');
	}

	public function index()
	{
		return false;
	}


	public function getProfile()
	{
		/*
		http://service.mobitrackbd.com/authentication/getVehicles
		
		Final
		{"user_login_email":"mehedi247@hotmail.com","user_password":"sheops1"}
		*/

		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			// customer_id
			if (isset($data['customer_id'])) {
				$_POST['customer_id'] = $data['customer_id'];
			}
			if (isset($data['user_id'])) {
				$_POST['user_id'] = $data['user_id'];
			}

			//print_r($data);

			//--------------------------------------------------------------------------------------------------------------

			// validation security 
			$this->form_validation->set_rules("customer_id", "Customer ID", "required");
			$this->form_validation->set_rules("user_id", "User ID", "required");
			if ($this->form_validation->run() == FALSE) {
				// show error
				$response['message'] = "Invalid Customer ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			} else {
				$this->customer_model->getProfile($data['customer_id'], $data['user_id']);
			}
		}
	}

	public function updateProfile()
	{
		/*
		http://service.mobitrackbd.com/authentication/getVehicles
		
		Final
		{"user_login_email":"mehedi247@hotmail.com","user_password":"sheops1"}
		*/

		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			// customer_id
			if (isset($data['customer_id'])) {
				$_POST['customer_id'] = $data['customer_id'];
			}
			if (isset($data['user_id'])) {
				$_POST['user_id'] = $data['user_id'];
			}
			if (isset($data['vehicle_id'])) {
				$_POST['vehicle_id'] = $data['vehicle_id'];
			}
			if (isset($data['vehicle_max_speed'])) {
				$_POST['vehicle_max_speed'] = $data['vehicle_max_speed'];
			}
			if (isset($data['vehicle_driver_name'])) {
				$_POST['vehicle_driver_name'] = $data['vehicle_driver_name'];
			}
			if (isset($data['vehicle_driver_license'])) {
				$_POST['vehicle_driver_license'] = $data['vehicle_driver_license'];
			}
			if (isset($data['vehicle_address'])) {
				$_POST['vehicle_address'] = $data['vehicle_address'];
			}
			if (isset($data['vehicle_driver_phone'])) {
				$_POST['vehicle_driver_phone'] = $data['vehicle_driver_phone'];
			}
			if (isset($data['vehicle_distence_per_unit'])) {
				$_POST['vehicle_distence_per_unit'] = $data['vehicle_distence_per_unit'];
			}
			//print_r($data);

			// validation security 
			$this->form_validation->set_rules("customer_id", "Customer ID", "required");
			$this->form_validation->set_rules("user_id", "User ID", "required");
			$this->form_validation->set_rules("vehicle_id", "Vehicle ID", "required");
			$this->form_validation->set_rules("vehicle_max_speed", "Speed ID", "trim");
			$this->form_validation->set_rules("vehicle_driver_name", "Driver Name", "trim");
			$this->form_validation->set_rules("vehicle_driver_license", "License", "trim");
			$this->form_validation->set_rules("vehicle_address", "Address", "trim");
			$this->form_validation->set_rules("vehicle_driver_phone", "Phone", "trim");
			$this->form_validation->set_rules("vehicle_distence_per_unit", "Kilometer Per Liter", "trim");
			if ($this->form_validation->run() == FALSE) {
				// show error
				$response['message'] = "Invalid Customer ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			} else {
				$this->customer_model->updateProfile($data['customer_id'], $data['user_id'],  $data['vehicle_id']);
			}
		}
	}
}
