<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Task extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 
		$this->load->model('task_model');
	}

	public function index()
	{
		return false;
	}

	public function getTaskForMe()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['customer_id'])) {
					$_POST['customer_id'] = $data['customer_id'];
				}
				if (isset($data['project_id'])) {
					$_POST['project_id'] = $data['project_id'];
				}
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("customer_id", "Customer ID", "required");
				$this->form_validation->set_rules("project_id", "Project ID", "required");
				$this->form_validation->set_rules("user_id", "User ID", "required");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid Customer ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->task_model->getTaskForMe($data['customer_id'], $data['project_id'], $data['user_id']);
				}
			} else {
				// show error
				$response['message'] = "Invalid Customer ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function getTaskIAssigned()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['customer_id'])) {
					$_POST['customer_id'] = $data['customer_id'];
				}
				if (isset($data['project_id'])) {
					$_POST['project_id'] = $data['project_id'];
				}
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("customer_id", "Customer ID", "required");
				$this->form_validation->set_rules("project_id", "Project ID", "required");
				$this->form_validation->set_rules("user_id", "User ID", "required");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid Customer ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->task_model->getTaskIAssigned($data['customer_id'], $data['project_id'], $data['user_id']);
				}
			} else {
				// show error
				$response['message'] = "Invalid Customer ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}


	public function getTaskDetailsById()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				if (isset($data['followup_id'])) {
					$_POST['followup_id'] = $data['followup_id'];
				}

				// validation security 
				$this->form_validation->set_rules("user_id", "User ID", "required");
				$this->form_validation->set_rules("followup_id", "Task ID", "required");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid Customer ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->task_model->getTaskDetailsById($data['user_id'], $data['followup_id']);
				}
			} else {
				// show error
				$response['message'] = "Invalid User ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}


	public function getUserListByCustomerIdUserId()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {

			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['customer_id'])) {
					$_POST['customer_id'] = $data['customer_id'];
				}
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("customer_id", "Customer ID", "required");
				$this->form_validation->set_rules("user_id", "User ID", "required");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid Customer ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->task_model->getUserListByCustomerIdUserId($data['customer_id'], $data['user_id']);
				}
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function updateFCMUserId()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {

			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				if (isset($data['uu_id'])) {
					$_POST['uu_id'] = $data['uu_id'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("user_id", "User ID", "required");
				$this->form_validation->set_rules("uu_id", "UUID", "required");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid User ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->task_model->updateFCMUserId($data['user_id'], $data['uu_id']);
				}
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function addTask()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {

			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				if (isset($data['customer_id'])) {
					$_POST['customer_id'] = $data['customer_id'];
				}
				if (isset($data['project_id'])) {
					$_POST['project_id'] = $data['project_id'];
				}
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				if (isset($data['followup_for'])) {
					$_POST['followup_for'] = $data['followup_for'];
				}
				if (isset($data['followup_date'])) {
					$_POST['followup_date'] = $data['followup_date'];
				}
				if (isset($data['followup_from_time'])) {
					$_POST['followup_from_time'] = $data['followup_from_time'];
				}
				if (isset($data['followup_to_time'])) {
					$_POST['followup_to_time'] = $data['followup_to_time'];
				}
				if (isset($data['followup_title'])) {
					$_POST['followup_title'] = $data['followup_title'];
				}
				if (isset($data['followup_description'])) {
					$_POST['followup_description'] = $data['followup_description'];
				}
				if (isset($data['followup_priority'])) {
					$_POST['followup_priority'] = $data['followup_priority'];
				}

				// validation security 
				$this->form_validation->set_rules("customer_id", "Customer ID", "required");
				$this->form_validation->set_rules("project_id", "Project ID", "required");
				$this->form_validation->set_rules("user_id", "User ID", "required");
				$this->form_validation->set_rules("followup_for", "Followup For", "required");
				$this->form_validation->set_rules("followup_date", "Followup Date", "trim");
				$this->form_validation->set_rules("followup_from_time", "Followup From Time", "trim");
				$this->form_validation->set_rules("followup_to_time", "Followup To Time", "trim");
				$this->form_validation->set_rules("followup_title", "Followup Title", "trim");
				$this->form_validation->set_rules("followup_description", "Followup Description", "trim");
				$this->form_validation->set_rules("followup_priority", "Followup Priority", "trim");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid User ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->task_model->addTask();
				}
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function editTask()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {

			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				if (isset($data['customer_id'])) {
					$_POST['customer_id'] = $data['customer_id'];
				}
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				if (isset($data['followup_id'])) {
					$_POST['followup_id'] = $data['followup_id'];
				}
				if (isset($data['followup_for'])) {
					$_POST['followup_for'] = $data['followup_for'];
				}
				if (isset($data['followup_date'])) {
					$_POST['followup_date'] = $data['followup_date'];
				}
				if (isset($data['followup_from_time'])) {
					$_POST['followup_from_time'] = $data['followup_from_time'];
				}
				if (isset($data['followup_to_time'])) {
					$_POST['followup_to_time'] = $data['followup_to_time'];
				}
				if (isset($data['followup_title'])) {
					$_POST['followup_title'] = $data['followup_title'];
				}
				if (isset($data['followup_description'])) {
					$_POST['followup_description'] = $data['followup_description'];
				}
				if (isset($data['followup_priority'])) {
					$_POST['followup_priority'] = $data['followup_priority'];
				}
				if (isset($data['followup_status'])) {
					$_POST['followup_status'] = $data['followup_status'];
				}

				// validation security 
				$this->form_validation->set_rules("customer_id", "Customer ID", "required");
				$this->form_validation->set_rules("user_id", "User ID", "required");
				$this->form_validation->set_rules("followup_id", "Followup ID", "required");
				$this->form_validation->set_rules("followup_for", "Followup For", "required");
				$this->form_validation->set_rules("followup_date", "Followup Date", "trim");
				$this->form_validation->set_rules("followup_from_time", "Followup From Time", "trim");
				$this->form_validation->set_rules("followup_to_time", "Followup To Time", "trim");
				$this->form_validation->set_rules("followup_title", "Followup Title", "trim");
				$this->form_validation->set_rules("followup_description", "Followup Description", "trim");
				$this->form_validation->set_rules("followup_priority", "Followup Priority", "trim");
				$this->form_validation->set_rules("followup_status", "Followup Status", "trim");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid User ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->task_model->editTask();
				}
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function deleteTask()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				if (isset($data['customer_id'])) {
					$_POST['customer_id'] = $data['customer_id'];
				}
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				if (isset($data['followup_id'])) {
					$_POST['followup_id'] = $data['followup_id'];
				}

				// validation security 
				$this->form_validation->set_rules("customer_id", "Customer ID", "required");
				$this->form_validation->set_rules("user_id", "User ID", "required");
				$this->form_validation->set_rules("followup_id", "Followup ID", "required");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid User ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->task_model->deleteTask();
				}
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function changeFollowupStatus()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				if (isset($data['followup_id'])) {
					$_POST['followup_id'] = $data['followup_id'];
				}
				if (isset($data['followup_status'])) {
					$_POST['followup_status'] = $data['followup_status'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("user_id", "User ID", "required");
				$this->form_validation->set_rules("followup_id", "Followup Id", "required");
				$this->form_validation->set_rules("followup_status", "Followup Status", "required");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid User ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->task_model->changeFollowupStatus($data['user_id'], $data['followup_id'], $data['followup_status']);
				}
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}
}
