<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reportdm extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('report_new_model');
	}

	/**
	 * 
	 * 
	 */
	public function dailyReport()
	{
		$header = $this->input->request_headers();

		if ($header['app-token'] === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$api_data = json_decode(file_get_contents('php://input'), true);

			$deviceId = $api_data['device_id'];
			$date = $api_data['date'];

			$datas = $this->report_new_model->daily_report($deviceId, $date);

			if (empty($datas)) {
				$data = array(
					'datas' 			=> NULL,
					'status'			=> 204
				);

				header('Content-Type: application/json');
				echo json_encode($data);
			} else {
				$i = 0;
				$dataArray = array();
				$totalKm = 0;
				$subTotalKm = 0;
				$hour = 0;
				$lat = "";
				$lng = "";
				$maxSpeed = 0;

				while ($hour < 24) {
					for ($i; $i < count($datas); $i++) {
						if (strtotime(date('G:i', mktime($hour, 0, 0))) <= strtotime(date('G:i', strtotime($datas[$i]->server_time))) && strtotime(date('G:i', strtotime($datas[$i]->server_time))) < strtotime(date('G:i', mktime($hour + 1, 0, 0)))) {
							$totalKm += $datas[$i]->distance;
							$lat = $datas[$i]->lat;
							$lng = $datas[$i]->lng;

							if ($i == (count($datas) - 1)) {
								$resultKm = round($totalKm, 2);
								$subTotalKm += $resultKm;
								$totalKm = 0;
							}

							if ($speedIndex === 0) {
								$maxSpeed = $datas[$i]->speed;
								$speedIndex++;
							} else {
								if ($datas[$i]->speed > $maxSpeed) {
									$maxSpeed = $datas[$i]->speed;
								}
							}
						} else {
							$resultKm = round($totalKm, 2);
							$subTotalKm += $resultKm;
							$totalKm = 0;
							$speedIndex = 0;
							$totalKm += $datas[$i]->distance;
							break;
						}
					}

					$dataArray[] = [
						'start_time'    => date('G:iA', mktime($hour, 0, 0)),
						'end_time'      => date('G:iA', mktime($hour + 1, 0, 0)),
						'time_slot'     => $hour + 1,
						'distance'      => $resultKm,
						'lat' 			=> $lat,
						'lng' 			=> $lng,
						'max_speed'		=> $maxSpeed
					];

					$lat = "";
					$lng = "";
					$resultKm = 0;
					$maxSpeed = 0;
					$hour++;
				}

				$data = array(
					'data' 				=> $dataArray,
					'total_distance'	=> round($subTotalKm, 2),
					'status'			=> 200
				);

				header('Content-Type: application/json');
				echo json_encode($data);
			}
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	/**
	 * 
	 * 
	 */

	public function monthlyReport()
	{
		$header = $this->input->request_headers();

		if ($header['app-token'] === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$api_data = json_decode(file_get_contents('php://input'), true);

			$deviceId 	= $api_data['device_id'];
			$date 		= $api_data['date'];
			$month 		= date("m", strtotime($date));
			$year 		= date("Y", strtotime($date));

			$datas = $this->report_new_model->monthly_report($deviceId, $month, $year);

			if (empty($datas)) {
				$data = array(
					'datas' 			=> NULL,
					'status'			=> 204
				);

				header('Content-Type: application/json');
				echo json_encode($data);
			} else {
				$loopDate = date('Y-m-d', strtotime($datas[0]->server_time));
				$datedArray = array();
				$totalKm = 0;
				$subTotalKm = 0;
				$speedIndex = 0;
				$maxSpeed = 0;
				$totalSpeed = 0;


				for ($i = 0; $i < count($datas); $i++) {
					if ($loopDate == date('Y-m-d', strtotime($datas[$i]->server_time))) {
						$totalKm += $datas[$i]->distance;
						$totalSpeed += $datas[$i]->speed;

						if ($speedIndex === 0) {
							$maxSpeed = $datas[$i]->speed;
							$speedIndex++;
						} else {
							if ($datas[$i]->speed > $maxSpeed) {
								$maxSpeed = $datas[$i]->speed;
							}
							$speedIndex++;
						}
					} else {
						$datedArray[] = [
							'date'			=> $loopDate,
							'max_speed'		=> $maxSpeed,
							'average_speed'	=> round(($totalSpeed / $speedIndex), 2),
							'distance'		=> round($totalKm, 2)
						];
						$subTotalKm += $totalKm;
						$totalKm = 0;
						$speedIndex = 0;
						$totalSpeed = 0;
						$loopDate = date('Y-m-d', strtotime($datas[$i]->server_time));
					}
				}

				$data = array(
					'data' 				=> $datedArray,
					'total_distance'	=> round($subTotalKm, 2),
					'status'			=> 200
				);

				header('Content-Type: application/json');
				echo json_encode($data);
			}
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}
}
