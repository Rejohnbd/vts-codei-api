<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Device extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 
		$this->load->model('device_model');
	}

	public function index()
	{
		return false;
	}


	public function inquiryDataByDeviceId()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			// device_id
			if (isset($data['device_id'])) {
				$_POST['device_id'] = $data['device_id'];
			}

			// validation security 
			$this->form_validation->set_rules("device_id", "Device ID", "required");
			if ($this->form_validation->run() == FALSE) {
				// show error
				$response['message'] = "Invalid Device ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			} else {
				$this->device_model->inquiryDataByDeviceId($data['device_id']);
			}
		}
	}

	public function downloadDataByDeviceId()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			// device_id
			if (isset($data['device_id'])) {
				$_POST['device_id'] = $data['device_id'];
			}

			// validation security 
			$this->form_validation->set_rules("device_id", "Device ID", "required");
			if ($this->form_validation->run() == FALSE) {
				// show error
				$response['message'] = "Invalid Device ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			} else {
				$this->device_model->downloadDataByDeviceId();
			}
		}
	}
}
