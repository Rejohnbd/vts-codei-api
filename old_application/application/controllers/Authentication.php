<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 
		$this->load->model('authentication_model');
	}

	public function index()
	{
		//return false;
	}

	public function authenticate()
	{
		if ($this->input->get_request_header('app-token', TRUE) == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// imei
				if (isset($data['user_login_email'])) {
					$_POST['user_login_email'] = $data['user_login_email'];
				}
				// Lat
				if (isset($data['user_password'])) {
					$_POST['user_password'] = $data['user_password'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("user_login_email", "Email", "valid_email|required");
				$this->form_validation->set_rules("user_password", "Pass", "trim|required");
				if ($this->form_validation->run() == FALSE) {
					header('Content-Type: application/json');
					$response['message'] = "Validation failed!";
					$test[] = $response;
					echo json_encode($test);
				} else {
					$this->authentication_model->authenticate();
				}
			} else {
				header('Content-Type: application/json');
				$response['message'] = "Invalid Inputs!";
				$test[] = $response;
				echo json_encode($test);
				return false;
			}
		} else {
			$response = array(
				'user' => 'No user found!',
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}
}
