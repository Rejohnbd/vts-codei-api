<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vehicle extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 
		$this->load->model('authentication_model');
	}

	public function index()
	{
		return false;
	}


	public function getAllVehiclesByCustomerID()
	{
		if ($this->input->get_request_header('app-token', TRUE) == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['customer_id'])) {
					$_POST['customer_id'] = $data['customer_id'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("customer_id", "Customer ID", "required");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid Customer ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->authentication_model->getAllVehiclesByCustomerId($data['customer_id']);
				}
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function getLastCoordinate()
	{
		if ($this->input->get_request_header('app-token', TRUE) == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['device_id'])) {
					$_POST['device_id'] = $data['device_id'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("device_id", "Device ID", "required");
				if ($this->form_validation->run() == FALSE) {
					$response['message'] = "Validation Failed!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->authentication_model->getLastCoordinatesByDeviceId($data['device_id']);
				}
			} else {
				$response['message'] = "Invalid Inputs!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
				return false;
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function getLastCoordinateDemoGet()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$this->load->model('fake_model');
			/*
			http://service.mobitrackbd.com/authentication/getLastCoordinate

			Final
			{"vehicle_id":"1"}
			*/

			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['device_id'])) {
					$_POST['device_id'] = $data['device_id'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("device_id", "Device ID", "required");
				if ($this->form_validation->run() == FALSE) {
					$response['message'] = "Validation Failed!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->fake_model->getFakeData($data['device_id']);
				}
			} else {
				$response['message'] = "Invalid Inputs!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
				return false;
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function getLastCoordinateDemoPost()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {

			$this->load->model('fake_model');

			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['device_id'])) {
					$_POST['device_id'] = $data['device_id'];
				}
				if (isset($data['device_imei'])) {
					$_POST['device_imei'] = $data['device_imei'];
				}
				if (isset($data['server_time'])) {
					$_POST['server_time'] = $data['server_time'];
				}
				if (isset($data['device_time'])) {
					$_POST['device_time'] = $data['device_time'];
				}
				if (isset($data['lat'])) {
					$_POST['lat'] = $data['lat'];
				}
				if (isset($data['lng'])) {
					$_POST['lng'] = $data['lng'];
				}
				if (isset($data['distance'])) {
					$_POST['distance'] = $data['distance'];
				}
				if (isset($data['speed'])) {
					$_POST['speed'] = $data['speed'];
				}
				if (isset($data['engine_status'])) {
					$_POST['engine_status'] = $data['engine_status'];
				}
				if (isset($data['course'])) {
					$_POST['course'] = $data['course'];
				}
				if (isset($data['address'])) {
					$_POST['address'] = $data['address'];
				}
				if (isset($data['attributes'])) {
					$_POST['attributes'] = $data['attributes'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("device_id", "Position ID", "required");
				$this->form_validation->set_rules("device_imei", "Position ID", "required");
				$this->form_validation->set_rules("server_time", "Position ID", "required");
				$this->form_validation->set_rules("device_time", "Position ID", "required");
				$this->form_validation->set_rules("lat", "Position ID", "required");
				$this->form_validation->set_rules("lng", "Position ID", "required");
				$this->form_validation->set_rules("distance", "Position ID", "required");
				$this->form_validation->set_rules("speed", "Position ID", "required");
				$this->form_validation->set_rules("engine_status", "Position ID", "required");
				$this->form_validation->set_rules("course", "Position ID", "required");
				$this->form_validation->set_rules("address", "Position ID", "required");
				$this->form_validation->set_rules("attributes", "Position ID", "required");
				if ($this->form_validation->run() == FALSE) {
					$response['message'] = "Validation Failed!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->fake_model->addFakeData();
				}
			} else {
				$response['message'] = "Invalid Inputs!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
				return false;
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}
}
