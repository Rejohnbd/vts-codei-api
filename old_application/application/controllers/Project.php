<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		$this->load->model('project_model');
	}

	public function index()
	{
		return false;
	}

	public function getAllProject()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// converting json data to post
				/*{"user_id":"1","project_name":"project_name","project_description":"project_description","project_percent":"project_percent","project_status":"project_status","project_start_date":"project_start_date","project_complete_date":"project_complete_date",}
				*/
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				//print_r($data);

				// validation security 
				$this->form_validation->set_rules('user_id', 'User ID', 'required');

				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Validation Failed!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->project_model->getAllProject();
				}
			} else {
				// show error
				$response['message'] = "Invalid Data!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$response['message'] = 'Invalid Request';
			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function getProjectDetailsByProjectId()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// converting json data to post
				/* {"user_id":"1","project_name":"project_name","project_description":"project_description","project_percent":"project_percent","project_status":"project_status","project_start_date":"project_start_date","project_complete_date":"project_complete_date",} */
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				if (isset($data['project_id'])) {
					$_POST['project_id'] = $data['project_id'];
				}
				//print_r($data);

				if (isset($data['project_name'])) {
					$_POST['project_name'] = $data['project_name'];
				}
				if (isset($data['project_description'])) {
					$_POST['project_description'] = $data['project_description'];
				}
				if (isset($data['project_percent'])) {
					$_POST['project_percent'] = $data['project_percent'];
				}
				if (isset($data['project_status'])) {
					$_POST['project_status'] = $data['project_status'];
				}
				if (isset($data['project_start_date'])) {
					$_POST['project_start_date'] = $data['project_start_date'];
				}
				if (isset($data['project_complete_date'])) {
					$_POST['project_complete_date'] = $data['project_complete_date'];
				}


				// validation security 
				$this->form_validation->set_rules('project_id', 'Project ID', 'required');
				$this->form_validation->set_rules('project_name', 'project_name', 'required');
				$this->form_validation->set_rules('project_description', 'project_description', 'required');
				$this->form_validation->set_rules('project_percent', 'project_percent', 'required');
				$this->form_validation->set_rules('project_status', 'project_status', 'required');
				$this->form_validation->set_rules('project_start_date', 'project_start_date', 'required');
				$this->form_validation->set_rules('project_complete_date', 'project_complete_date', 'required');

				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->task_model->getTaskDetailsByTaskId($data['project_id']);
				}
			} else {
				// show error
				$response['message'] = "Invalid ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$response = array(
				'message' => 'Invalid Request'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function addProject()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// Converting json to post data.
				/*{"user_id":"1","project_name":"project_name","project_description":"project_description","project_percent":"project_percent","project_status":"project_status","project_start_date":"project_start_date","project_complete_date":"project_complete_date",}*/
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				if (isset($data['project_name'])) {
					$_POST['project_name'] = $data['project_name'];
				}
				if (isset($data['project_description'])) {
					$_POST['project_description'] = $data['project_description'];
				}
				if (isset($data['project_start_date'])) {
					$_POST['project_start_date'] = $data['project_start_date'];
				}
				if (isset($data['project_complete_date'])) {
					$_POST['project_complete_date'] = $data['project_complete_date'];
				}

				//print_r($data);

				// validation security 
				$this->form_validation->set_rules('user_id', 'User ID', 'required');
				$this->form_validation->set_rules('project_name', 'project_name', 'required');
				$this->form_validation->set_rules('project_description', 'project_description', 'required');
				$this->form_validation->set_rules('project_start_date', 'project_start_date', 'required');
				$this->form_validation->set_rules('project_complete_date', 'project_complete_date', 'required');

				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Validation Failed!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->project_model->addProject();
				}
			} else {
				$response['message'] = "Invalid Data!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$response = array(
				'message' => 'Invalid Request!'
			);
			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function editProject()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// Converting json to post data.
				/*{"user_id":"1","project_id":"1","project_name":"project_name","project_description":"project_description","project_percent":"project_percent","project_status":"project_status","project_start_date":"project_start_date","project_complete_date":"project_complete_date",}*/

				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				if (isset($data['project_id'])) {
					$_POST['project_id'] = $data['project_id'];
				}
				if (isset($data['project_name'])) {
					$_POST['project_name'] = $data['project_name'];
				}
				if (isset($data['project_description'])) {
					$_POST['project_description'] = $data['project_description'];
				}
				if (isset($data['project_start_date'])) {
					$_POST['project_start_date'] = $data['project_start_date'];
				}
				if (isset($data['project_complete_date'])) {
					$_POST['project_complete_date'] = $data['project_complete_date'];
				}

				//print_r($data);

				// validation security 
				$this->form_validation->set_rules('project_id', 'Project ID', 'required');
				$this->form_validation->set_rules('user_id', 'User ID', 'required');
				$this->form_validation->set_rules('project_name', 'Project Name', 'required');
				$this->form_validation->set_rules('project_description', 'Project Description', 'required');
				$this->form_validation->set_rules('project_start_date', 'Project Start Date', 'required');
				$this->form_validation->set_rules('project_complete_date', 'Project Complete Date', 'required');

				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->project_model->editProject();
				}
			}
		} else {
			$response = array(
				'message' => 'Invalid Request'
			);
			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function deleteProject()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				/*{"user_id":"1","project_id":"1","project_name":"project_name","project_description":"project_description","project_percent":"project_percent","project_status":"project_status","project_start_date":"project_start_date","project_complete_date":"project_complete_date",}*/
				if (isset($data['user_id'])) {
					$_POST['user_id'] = $data['user_id'];
				}
				if (isset($data['project_id'])) {
					$_POST['project_id'] = $data['project_id'];
				}

				//print_r($data);

				// validation security 
				$this->form_validation->set_rules('project_id', 'Project ID', 'required');
				$this->form_validation->set_rules('user_id', 'User ID', 'required');

				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->project_model->deleteProject();
				}
			}
		} else {
			$response = array(
				'message' => 'Invalid Request'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}
}
