<?php class Hourly_report_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getDailyHourlyReport()
	{
		$response = '';
		$device_id = $this->input->post('device_id');
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$device_id = $this->input->post('device_id');
		$vehicle_distence_per_unit = $this->input->post('vehicle_distence_per_unit');
		
		$today = $this->input->post('date');
		
		if(isset($device_id) && $device_id!=NULL)
		{
			$fromDateTime = $today.' '.$from;
			$toDateTime = $today.' '.$to;

			$returnData['distance'] = 0;

			$this->db->select_sum('distance');
			$this->db->where('device_id', $device_id);
			$this->db->where('server_time >=', date('Y-m-d H:i:s', strtotime($fromDateTime)));
			$this->db->where('server_time <=', date('Y-m-d H:i:s', strtotime($toDateTime)));
			$this->db->limit(500);
			$query = $this->db->get('tta_position');
			if ($query->num_rows() > 0)
			{
				foreach($query->result() as $row)
				{
					$returnData['distance'] = $row->distance;
				}
			}
			else
			{
				$returnData['distance'] = 0;
				//$returnData['address'] = "";
			}
			header('Content-Type: application/json');
			echo json_encode($returnData);
			
		}
	}
	
	
	
}