<?php class Device_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	
	// Old Function
	function getDeviceInfoByImei($device_imei="")
	{
		$this->db->where('device_imei', $device_imei);
		$this->db->where('device_status', 1);
		$query = $this->db->get('tta_devices');
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$result['device_id'] = $row->device_id;
				$result['device_type'] = $row->device_type;
				$result['vehicle_id'] = $row->vehicle_id;
				$result['customer_id'] = $row->customer_id;
				$result['device_imei'] = $row->device_imei;
				$result['device_unique_id'] = $row->device_unique_id;
				$result['device_activation_status'] = $row->device_activation_status;
				$result['device_activated_by'] = $row->device_activated_by;
				$result['device_activated_date_time'] = $row->device_activated_date_time;
				$result['device_deactivated_by'] = $row->device_deactivated_by;
				$result['device_deactivated_date_time'] = $row->device_deactivated_date_time;
				$result['device_authorization_code'] = $row->device_authorization_code;
				$result['device_authorizad_by'] = $row->device_authorizad_by;
				$result['device_authorized_date_time'] = $row->device_authorized_date_time;
				$result['device_deauthorized_by'] = $row->device_deauthorized_by;
				$result['device_deauthorized_date_time'] = $row->device_deauthorized_date_time;
				$result['device_asigned_status'] = $row->device_asigned_status;
				$result['device_asigned_to'] = $row->device_asigned_to;
				$result['device_asigned_date'] = $row->device_asigned_date;
				$result['device_sim_number'] = $row->device_sim_number;
				$result['device_current_url'] = $row->device_current_url;
				$result['device_old_url'] = $row->device_old_url;
				$result['device_update_url'] = $row->device_update_url;
				$result['device_varified'] = $row->device_varified;
				$result['device_firmware'] = $row->device_firmware;
				$result['device_version'] = $row->device_version;
				$result['device_sleep_interval'] = $row->device_sleep_interval;
				$result['device_note'] = $row->device_note;
				$result['device_status'] = $row->device_status;
				$result['device_inserted_by'] = $row->device_inserted_by;
				$result['device_insert_date_time'] = $row->device_insert_date_time;
				$result['device_updated_by'] = $row->device_updated_by;
				$result['device_update_date_time'] = $row->device_update_date_time;
			}
			return $result; 
		}
		else
		{
			return false;
		}	
	}
	
	
	function inquiryDataByDeviceId()
	{
		$device_id = $this->input->post('device_id');
		
		$this->db->where('device_id', $device_id);
		$query = $this->db->get('tta_position');
		if($query->num_rows() > 0)
		{
			$this->load->dbutil();
			echo $this->dbutil->csv_from_result($query);
		}
		else
		{
			$response['Total Data'] = '0';
			$test[] = $response;
			header('Content-Type: application/json');
			echo json_encode( $test );
		}
	}
	
	function downloadDataByDeviceId()
	{
		$device_id = $this->input->post('device_id');
		
		$this->db->where('device_id', $device_id);
		$query = $this->db->get('tta_position');
		if($query->num_rows() > 0)
		{
			 $this->load->dbutil();
			$this->load->helper('file');
			$this->load->helper('download');
			$delimiter = ",";
			$newline = "\r\n";
			$data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
			force_download('CSV_Report.csv', $data);
		}
		else
		{
			$response['Total Data'] = '0';
			$test[] = $response;
			header('Content-Type: application/json');
			echo json_encode( $test );
		}
	}
}