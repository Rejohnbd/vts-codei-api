<?php class Customer_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function getProfile($customer_id='', $user_id='')
	{
		if(isset($customer_id) && $customer_id!=NULL && isset($user_id) && $user_id!=NULL)
		{
			$this->db->where('customer_id', $customer_id);
			$query = $this->db->get('tta_customers');
			if($query->num_rows() == 1)
			{
				foreach ($query->result() as $row)
				{
					$result['customer_name'] = $row->customer_name;
					$result['customer_primary_email'] = $row->customer_primary_email;
					$result['customer_billing_email'] = $row->customer_billing_email;
					$result['customer_primary_phone'] = $row->customer_primary_phone;
					$result['customer_secondary_phone'] = $row->customer_secondary_phone;
					$result['customer_address'] = $row->customer_address;
				}
				$test['customer_info'] = $result;
				
				$this->db->where('customer_id', $customer_id);
				$this->db->where('user_id', $user_id);
				$query1 = $this->db->get('tta_users');
				if($query1->num_rows() == 1)
				{
					foreach ($query1->result() as $row1)
					{
						$result1['user_name'] = $row1->user_name;
						$result1['primary_phone'] = $row1->primary_phone;
						$result1['cell_phone'] = $row1->cell_phone;
					}
				}
				else
				{
					$result1['user_info'] = "Invalid User!";
				}
				
				$test['user_info'] = $result1;
				
				$test['message'] = "Customer Information";
				
				$x[] = $test;
				header('Content-Type: application/json');
				echo json_encode($x);
			}
			else
			{
				$test['message'] = "No Customer Found!";
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		}
		else
		{
			$test['message'] = "Invalid Customer ID!";
				header('Content-Type: application/json');
				echo json_encode($test);
		}
	}
	
	function updateProfile($customer_id='', $user_id='', $vehicle_id='')
	{
		if(isset($customer_id) && $customer_id!=NULL && isset($user_id) && $user_id!=NULL && isset($vehicle_id) && $vehicle_id!=NULL)
		{
			$this->db->where('customer_id', $customer_id);
			$this->db->where('vehicle_id', $vehicle_id);
			$this->db->where('vehicle_status', 1);
			$query = $this->db->get('tta_vehicles');
			if($query->num_rows() == 1)
			{
				$data ['vehicle_max_speed'] = $this->input->post('vehicle_max_speed'); 
				$data ['vehicle_driver_name'] = $this->input->post('vehicle_driver_name'); 
				$data ['vehicle_fathers_name'] = $this->input->post('vehicle_fathers_name'); 
				$data ['vehicle_driver_license'] = $this->input->post('vehicle_driver_license'); 
				$data ['vehicle_address'] = $this->input->post('vehicle_address'); 
				$data ['vehicle_driver_phone'] = $this->input->post('vehicle_driver_phone'); 
				//$data ['vehicle_driver_dob'] = date('Y-m-d', strtotime($this->input->post('vehicle_driver_dob'))); 
				$data ['vehicle_updated_by'] = $user_id;
				$data ['vehicle_update_date_time'] = date('Y-m-d H:i:s');
				$data ['vehicle_distence_per_unit'] = $this->input->post('vehicle_distence_per_unit');

				$this->db->where('vehicle_id', $vehicle_id);
				$this->db->where('customer_id', $customer_id); // for hacking protection
				if($this->db->update('tta_vehicles', $data))
				{
					$test['message'] = "Updated Successfully.";
					header('Content-Type: application/json');
					echo json_encode($test);
				}
				else
				{
					$test['message'] = "Updated Failed!";
					header('Content-Type: application/json');
					echo json_encode($test);
				}
			}
			else
			{
				$test['message'] = "Invalid Vehicle ID!";
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		}
		else
		{
			$test['message'] = "Invalid Customer ID!";
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}
}