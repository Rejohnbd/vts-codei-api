<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_new_model extends CI_Model {

	public function daily_report($device_id, $date)
	{	
		$this->db->select('position_id,server_time,lat,lng,distance,speed');
		$this->db->from('tta_position');
		$this->db->where('device_id', $device_id);
		$this->db->where('engine_status', 1);
		$this->db->where('DATE(server_time)', $date);

		$query_result = $this->db->get();
		$result = $query_result->result();
		return $result;
	}

	public function monthly_report($device_id, $month, $year)
	{
		$this->db->select('server_time,lat,lng,distance,speed');
		$this->db->from('tta_position');
		$this->db->where('device_id', $device_id);
		$this->db->where('engine_status', 1);
		$this->db->where('YEAR(server_time)', $year);
		$this->db->where('MONTH(server_time)', $month);

		$query_result = $this->db->get();
		$result = $query_result->result();
		return $result;
	}
}