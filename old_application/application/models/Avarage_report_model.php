<?php class Avarage_report_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function get24hrsAvarageReport()
	{
		$response = '';
		$device_id = $this->input->post('device_id');
		$today = date('Y-m-d', strtotime($this->input->post('date')));
		if(isset($device_id) && $device_id!=NULL)
		{
			/*$this->db->select_max('speed');
			$this->db->where('device_id', $device_id);
			$this->db->like('server_time', $today);
			$query = $this->db->get('tta_position');
			//print_r($query->result());
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $row)
				{
					$response['maxSpeed'] = $row->speed;
				}
				
			}
			else
			{
				$response['maxSpeed'] = 0;
			}
			*/
			
			$this->db->select_sum('distance');
			$this->db->where('device_id', $device_id);
			$this->db->like('server_time', $today);
			$query = $this->db->get('tta_position');
			//print_r($query->result());
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $row)
				{
					$response['totalDistance'] = number_format($row->distance, 2);
				}
				
			}
			else
			{
				$response['totalDistance'] = number_format(0, 2);
			}
			
			$speedAdded = 0;
			$this->db->select_sum('speed');
			$this->db->where('device_id', $device_id);
			$this->db->like('server_time', $today);
			$query1 = $this->db->get('tta_position');
			$numberOfRows = $query1->num_rows();
			if($numberOfRows > 0)
			{
				foreach($query1->result() as $row1)
				{
					$speedAdded = $row1->speed;
				}
			}
			
			$this->db->select('speed');
			$this->db->where('device_id', $device_id);
			$this->db->like('server_time', $today);
			$query2 = $this->db->get('tta_position');
			$numberOfRows = $query2->num_rows();
			
			
			if(isset($numberOfRows) && $numberOfRows!=NULL)
			{
				$response['avarageSpeed'] = number_format($speedAdded / $numberOfRows, 2);
			}
			else
			{
				$response['avarageSpeed'] = number_format(0, 2);
			}
			
			$newResponse['deviceData'] = $response;
			$newResponse['message'] = "Successful.";
			
			header('Content-Type: application/json');
			$dataX[] = $newResponse;
			echo json_encode($dataX);
			
		}
		else
		{
			$response['message'] = "Invalid Device!";
			$test[] = $response;
			header('Content-Type: application/json');
			echo json_encode( $test );
		}
		
	}
		
		
	function getMonthlyAvarageReport()
	{
		$response = '';
		$device_id = $this->input->post('device_id');
		$month_year = $this->input->post('month_year');
		$year = $this->input->post('year');
		if(isset($device_id) && $device_id!=NULL)
		{
			// Vulval checkup
			
			$today = date('Y-m-', strtotime("01-".$month_year));
			
			$this->db->select_sum('distance');
			$this->db->where('device_id', $device_id);
			$this->db->like('server_time', $today);
			$query = $this->db->get('tta_position');
			//print_r($query->result());
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $row)
				{
					$response['totalDistance'] = number_format($row->distance, 2);
				}
				
			}
			else
			{
				$response['totalDistance'] = number_format(0, 2);
			}
			
			$speedAdded = 0;
			$this->db->select_sum('speed');
			$this->db->where('device_id', $device_id);
			$this->db->like('server_time', $today);
			$query1 = $this->db->get('tta_position');
			$numberOfRows = $query1->num_rows();
			if($numberOfRows > 0)
			{
				foreach($query1->result() as $row1)
				{
					$speedAdded = $row1->speed;
				}
			}
			
			$this->db->select('speed');
			$this->db->where('device_id', $device_id);
			$this->db->like('server_time', $today);
			$query2 = $this->db->get('tta_position');
			$numberOfRows = $query2->num_rows();
			
			
			if(isset($numberOfRows) && $numberOfRows!=NULL)
			{
				$response['avarageSpeed'] = number_format($speedAdded / $numberOfRows, 2);
			}
			else
			{
				$response['avarageSpeed'] = number_format(0, 2);
			}
			
			$newResponse['deviceData'] = $response;
			$newResponse['message'] = "Successful.";
			
			header('Content-Type: application/json');
			$dataX[] = $newResponse;
			echo json_encode($dataX);
			
		}
		else
		{
			$response['message'] = "Invalid Device!";
			$test[] = $response;
			header('Content-Type: application/json');
			echo json_encode( $test );
		}
		
	}
	
	
	
}