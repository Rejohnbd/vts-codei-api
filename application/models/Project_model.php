<?php class Project_model extends CI_Model
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getAllProject()
	{
		$user_id = $this->input->post('user_id');
		if (isset($user_id) && $user_id != NULL) {
			$this->db->where('user_id', $user_id);
			$this->db->where('user_status', 1);
			$query1 = $this->db->get('tta_users');
			if ($query1->num_rows() == 1) {
				foreach ($query1->result() as $row) {
					$customer_id = $row->customer_id;
				}

				$this->db->where('user_id', $user_id);
				$this->db->where('customer_id', $customer_id);
				$this->db->where('status', 1);
				$query2 = $this->db->get('tta_project');
				if ($query2->num_rows() > 0) {
					$test1['all_project'] = $query2->result();
					$test1['message'] = "Project List";
					$x[] = $test1;
					header('Content-Type: application/json');
					echo json_encode($x);
				} else {
					$test1['message'] = "No Project Found!";
					header('Content-Type: application/json');
					echo json_encode($test1);
				}
			}
		} else {
			$test1['message'] = "Invalid ID!";
			header('Content-Type: application/json');
			echo json_encode($test1);
		}
	}

	function getProjectDetailsByProjectId($project_id = '')
	{
		if (isset($project_id) && $project_id != NULL) {
			$this->db->where('project_id', $project_id);
			$this->db->where('status', 1);
			$query = $this->db->get('tta_project');
			if ($query->num_rows() == 1) {
				$test1['project_details'] = $query->result();
				$test1['message'] = "Project Details";
				$x[] = $test1;
				header('Content-Type: application/json');
				echo json_encode($x);
			} else {
				$test1['message'] = "No Project Found!";
				header('Content-Type: application/json');
				echo json_encode($test1);
			}
		} else {
			$test1['message'] = "Invalid Project ID!";
			header('Content-Type: application/json');
			echo json_encode($test1);
		}
	}

	function addProject()
	{
		$user_id = $this->input->post('user_id');
		if (isset($user_id) && $user_id != NULL) {
			$this->db->where('user_id', $user_id);
			$this->db->where('user_status', 1);
			$query1 = $this->db->get('tta_users');
			if ($query1->num_rows() == 1) {
				foreach ($query1->result() as $row) {
					$customer_id = $row->customer_id;
				}
				$data["project_name"] = $this->input->post("project_name");
				$data["project_description"] = $this->input->post("project_description");
				$data["project_percent"] = 0;
				$data["project_status"] = 1;
				$data["project_start_date"] = date("Y-m-d", strtotime($this->input->post("project_start_date")));
				$data["project_complete_date"] = date("Y-m-d", strtotime($this->input->post("project_complete_date")));

				//print_r($data); 2021-12-31
				$data["user_id"] = $user_id;
				$data["customer_id"] = $customer_id;
				$data["insert_date_time"] = date("Y-m-d H:i:s");
				$data["inserted_by"] = $user_id;
				$data["update_date_time"] = date("Y-m-d H:i:s");
				$data["updated_by"] = $user_id;
				$data["status"] = 1;
				if ($this->db->insert("tta_project", $data)) {
					$test['message'] = "Added Successfully.";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				} else {
					$test['message'] = "Failed!";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				}
			} else {
				$test['message'] = "Invalid Customer ID!";
				$x[] = $test;
				header('Content-Type: application/json');
				echo json_encode($x);
			}
		} else {
			$test['message'] = "Invalid ID!";
			$x[] = $test;
			header('Content-Type: application/json');
			echo json_encode($x);
		}
	}

	function editProject()
	{
		$project_id = $this->input->post('project_id');
		$user_id = $this->input->post('user_id');
		if (isset($project_id) && $project_id != NULL && isset($user_id) && $user_id != NULL) {
			$this->db->where('user_id', $user_id);
			$this->db->where('user_status', 1);
			$query1 = $this->db->get('tta_users');
			if ($query1->num_rows() == 1) {
				foreach ($query1->result() as $row) {
					$customer_id = $row->customer_id;
				}
				$this->db->where('project_id', $project_id);
				$this->db->where('customer_id', $customer_id);
				$this->db->where('status', 1);
				$query = $this->db->get('tta_project');
				if ($query->num_rows() == 1) {
					// Converting json to post data.
					$data["project_name"] = $this->input->post("project_name");
					$data["project_description"] = $this->input->post("project_description");
					$data["project_percent"] = $this->input->post("project_percent");
					$data["project_status"] = $this->input->post("project_status");
					$data["project_start_date"] = $this->input->post("project_start_date");
					$data["project_complete_date"] = $this->input->post("project_complete_date");
					$data["update_date_time"] = date("Y-m-d H:i:s");
					$data["updated_by"] = $user_id;

					$this->db->where('project_id', $project_id);
					$this->db->where('customer_id', $customer_id);
					$this->db->where('status', 1);
					if ($this->db->update("tta_project", $data)) {
						$test['message'] = "Updated Successfully.";
						$x[] = $test;
						header('Content-Type: application/json');
						echo json_encode($x);
					} else {
						$test['message'] = "Failed!";
						$x[] = $test;
						header('Content-Type: application/json');
						echo json_encode($x);
					}
				} else {
					$test['message'] = "Invalid Project!";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				}
			} else {
				$test['message'] = "Invalid Customer ID!";
				$x[] = $test;
				header('Content-Type: application/json');
				echo json_encode($x);
			}
		} else {
			$test['message'] = "Invalid ID!";
			$x[] = $test;
			header('Content-Type: application/json');
			echo json_encode($x);
		}
	}

	function deleteProject()
	{
		$project_id = $this->input->post('project_id');
		$user_id = $this->input->post('user_id');
		if (isset($project_id) && $project_id != NULL && isset($user_id) && $user_id != NULL) {
			$this->db->where('project_id', $project_id);
			$this->db->where('status', 1);
			$query = $this->db->get('tta_project');
			if ($query->num_rows() == 1) {
				$data["status"] = '-1';
				$data["update_date_time"] = date("Y-m-d H:i:s");
				$data["updated_by"] = $user_id;

				$this->db->where('project_id', $project_id);
				$this->db->where('inserted_by', $user_id);
				if ($this->db->update("tta_project", $data)) {
					$test['message'] = "Deleted Successfully.";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				} else {
					$test['message'] = "Failed! Permission Denied!";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				}
			} else {
				$test['message'] = "Invalid Project ID!";
				$x[] = $test;
				header('Content-Type: application/json');
				echo json_encode($x);
			}
		} else {
			$test['message'] = "Invalid User ID!";
			$x[] = $test;
			header('Content-Type: application/json');
			echo json_encode($x);
		}
	}
}
