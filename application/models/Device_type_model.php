<?php
class Device_type_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function get_all_device_types()
    {
        $query_result = $this->db->get('tta_device_type');
        return $query_result->result();
    }

    public function create_device_type($data)
    {
        $createDeviceType = array(
            'device_type_name'      => $data['device_type_name'],
            'device_type_status'    => $data['device_type_status'],
        );
        $this->db->insert('tta_device_type', $createDeviceType);
    }

    public function check_device_type_exist($device_type_id)
    {
        $this->db->where('device_type', $device_type_id);
        $query_result = $this->db->get('tta_device_type');
        if ($query_result->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function get_device_type_by_id($device_type_id)
    {
        $this->db->where('device_type', $device_type_id);
        $query_result = $this->db->get('tta_device_type');
        return $query_result->row();
    }

    public function check_device_type_exist_by_name($device_type_name)
    {
        $this->db->where('device_type_name', $device_type_name);
        $query_result = $this->db->get('tta_device_type');
        if ($query_result->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function update_device_type_by_id($data)
    {
        $updateDeviceType = array(
            'device_type_name'      => $data['device_type_name'],
            'device_type_status'    => $data['device_type_status'],
        );
        $this->db->where('device_type', $data['device_type']);
        $this->db->update('tta_device_type', $updateDeviceType);

        $this->db->where('device_type', $data['device_type']);
        $query_result = $this->db->get('tta_device_type');
        return $query_result->row();
    }
}
