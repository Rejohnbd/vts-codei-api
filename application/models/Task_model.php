<?php class Task_model extends CI_Model
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getTaskForMe($customer_id = '', $project_id = '', $user_id = '')
	{
		if (isset($customer_id) && $customer_id != NULL && isset($project_id) && $project_id != NULL && isset($user_id) && $user_id != NULL) {
			$this->db->select('tta_followup.followup_id, tta_followup.customer_id, tta_followup.project_id, tta_followup.followup_for, tta_followup.followup_date, tta_followup.followup_from_time, tta_followup.followup_to_time, tta_followup.followup_title, tta_followup.followup_description, tta_followup.followup_priority, tta_followup.followup_status,	tta_followup.insert_date_time, tta_followup.inserted_by,  tta_users.user_name, tta_followup.update_date_time, tta_followup.updated_by');

			$this->db->from('tta_followup');
			$this->db->join('tta_users', 'tta_users.user_id = tta_followup.inserted_by');
			$this->db->where('tta_followup.customer_id', $customer_id);
			$this->db->where('tta_followup.project_id', $project_id);
			$this->db->where('tta_followup.followup_for', $user_id);
			$this->db->where('tta_followup.status', 1);
			$this->db->order_by('tta_followup.followup_date', 'DESC');
			$this->db->limit(100);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				header('Content-Type: application/json');

				$test1['all_task'] = $query->result();
				$test1['message'] = "Task List";

				$x[] = $test1;

				echo json_encode($x);
			} else {
				$test1['message'] = "No Task Found!";
				header('Content-Type: application/json');
				echo json_encode($test1);
			}
		} else {
			$test1['message'] = "Invalid Customer or User ID!";
			header('Content-Type: application/json');
			echo json_encode($test1);
		}
	}

	function getTaskIAssigned($customer_id = '', $project_id = '', $user_id = '')
	{
		if (isset($customer_id) && $customer_id != NULL && isset($project_id) && $project_id != NULL && isset($user_id) && $user_id != NULL) {
			$this->db->select('tta_followup.followup_id, tta_followup.customer_id, tta_followup.project_id, tta_followup.followup_for, tta_followup.followup_date, tta_followup.followup_from_time, tta_followup.followup_to_time, tta_followup.followup_title, tta_followup.followup_description, tta_followup.followup_priority, tta_followup.followup_status,	tta_followup.insert_date_time, tta_followup.inserted_by,  tta_users.user_name, tta_followup.update_date_time, tta_followup.updated_by');

			$this->db->from('tta_followup');
			$this->db->join('tta_users', 'tta_users.user_id = tta_followup.inserted_by');
			$this->db->where('tta_followup.customer_id', $customer_id);
			$this->db->where('tta_followup.project_id', $project_id);
			$this->db->where('tta_followup.inserted_by', $user_id);
			$this->db->where('tta_followup.status', 1);
			$this->db->order_by('tta_followup.followup_date', 'DESC');
			$this->db->limit(100);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				header('Content-Type: application/json');

				$test1['all_task'] = $query->result();
				$test1['message'] = "Task List";

				$x[] = $test1;

				echo json_encode($x);
			} else {
				$test1['message'] = "No Task Found!";
				header('Content-Type: application/json');
				echo json_encode($test1);
			}
		} else {
			$test1['message'] = "Invalid Customer or User ID!";
			header('Content-Type: application/json');
			echo json_encode($test1);
		}
	}

	function getTaskDetailsById($user_id = '', $followup_id = '')
	{
		if (isset($user_id) && $user_id != NULL && isset($followup_id) && $followup_id != NULL) {
			$this->db->select('tta_followup.followup_id, tta_followup.customer_id, tta_followup.followup_for, tta_followup.followup_date, tta_followup.followup_from_time, tta_followup.followup_to_time, tta_followup.followup_title, tta_followup.followup_description, tta_followup.followup_priority, tta_followup.followup_status,	tta_followup.insert_date_time, tta_followup.inserted_by,  tta_users.user_name, tta_followup.update_date_time, tta_followup.updated_by');

			$this->db->from('tta_followup');
			$this->db->join('tta_users', 'tta_users.user_id = tta_followup.inserted_by');
			$this->db->where('tta_followup.followup_id', $followup_id);
			$this->db->where('tta_followup.status', 1);
			$query = $this->db->get();
			if ($query->num_rows() == 1) {
				foreach ($query->result() as $row) {
					$followup_for = $row->followup_for;
					$this->db->where("user_id", $followup_for);
					$query1 = $this->db->get("tta_users");
					if ($query1->num_rows() == 1) {
						foreach ($query1->result() as $row1) {
							$followup_name = $row1->user_name;
						}
					}
					if (isset($followup_name) && $followup_name != NULL) {
						$followup_name = $followup_name;
					} else {
						$followup_name = "";
					}

					$taskInfo['followup_id'] = $row->followup_id;
					$taskInfo['customer_id'] = $row->customer_id;
					$taskInfo['followup_for_id'] = $row->followup_for;
					$taskInfo['followup_for_name'] = $followup_name;
					$taskInfo['followup_date'] = $row->followup_date;
					$taskInfo['followup_from_time'] = date("h:i A", strtotime($row->followup_from_time));
					$taskInfo['followup_to_time'] = date("h:i A", strtotime($row->followup_to_time));
					$taskInfo['followup_title'] = $row->followup_title;
					$taskInfo['followup_description'] = $row->followup_description;
					$taskInfo['followup_priority'] = $row->followup_priority;
					$taskInfo['followup_status'] = $row->followup_status;
					$taskInfo['insert_date_time'] = date("d-m-Y h:i A", strtotime($row->insert_date_time));
					$taskInfo['inserted_by'] = $row->inserted_by;
					$taskInfo['user_name'] = $row->user_name;
					$taskInfo['update_date_time'] = date("d-m-Y h:i A", strtotime($row->update_date_time));
					$taskInfo['updated_by'] = $row->updated_by;
				}
				header('Content-Type: application/json');
				$extraB[] = $taskInfo;
				$test1['task_details'] = $extraB;
				$test1['message'] = "Task Details";

				$x[] = $test1;

				echo json_encode($x);
			} else {
				$test1['message'] = "No Task Found!";
				header('Content-Type: application/json');
				echo json_encode($test1);
			}
		} else {
			$test1['message'] = "Invalid User ID!";
			header('Content-Type: application/json');
			echo json_encode($test1);
		}
	}

	function getUserListByCustomerIdUserId($customer_id = '', $user_id = '')
	{
		if (isset($customer_id) && $customer_id != NULL && isset($user_id) && $user_id != NULL) {
			$this->db->select("user_id, user_name, user_login_email, primary_phone, cell_phone");
			$this->db->where('customer_id', $customer_id);
			$this->db->where('user_status', 1);
			$query = $this->db->get('tta_users');
			if ($query->num_rows() > 0) {
				$test['all_user'] = $query->result();
				$test['message'] = "User List";

				$x[] = $test;
				header('Content-Type: application/json');
				echo json_encode($x);
			} else {
				$test['message'] = "No Task Found!";
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$test['message'] = "Invalid Customer or User ID!";
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}

	function updateFCMUserId($user_id = '', $uu_id = '')
	{
		if (isset($user_id) && $user_id != NULL && isset($uu_id) && $uu_id != NULL) {
			$this->db->where('user_id', $user_id);
			$this->db->where('user_status', 1);
			$query = $this->db->get('tta_users');
			if ($query->num_rows() == 1) {
				$data['uu_id'] = $uu_id;
				$this->db->where('user_id', $user_id);
				$this->db->where('user_status', 1);
				if ($this->db->update('tta_users', $data)) {
					$test['message'] = "Updated Successfully.";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				} else {
					$test['message'] = "Update failed!";
					header('Content-Type: application/json');
					echo json_encode($test);
				}
			} else {
				$test['message'] = "Invalid User!";
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$test['message'] = "Invalid User ID!";
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}

	function addTask()
	{
		$customer_id = $this->input->post('customer_id');
		$project_id = $this->input->post('project_id');
		$user_id = $this->input->post('user_id');

		if (isset($customer_id) && $customer_id != NULL && isset($project_id) && $project_id != NULL && isset($user_id) && $user_id != NULL) {
			$data["customer_id"] = $customer_id;
			$data["project_id"] = $project_id;
			$data["followup_for"] = $this->input->post("followup_for");
			$data["followup_date"] = date('Y-m-d', strtotime($this->input->post("followup_date")));
			$data["followup_from_time"] = date('H:i:s', strtotime($this->input->post("followup_from_time")));
			$data["followup_to_time"] = date('H:i:s', strtotime($this->input->post("followup_to_time")));
			$data["followup_title"] = $this->input->post("followup_title");
			$data["followup_description"] = $this->input->post("followup_description");
			$data["followup_priority"] = $this->input->post("followup_priority");
			$data["followup_status"] = 1;
			$data["insert_date_time"] = date("Y-m-d H:i:s");
			$data["inserted_by"] = $user_id;
			$data["update_date_time"] = date("Y-m-d H:i:s");
			$data["updated_by"] = $user_id;
			$data["status"] = 1;
			if ($this->db->insert("tta_followup", $data)) {
				$test['message'] = "Added Successfully.";
				$x[] = $test;
				header('Content-Type: application/json');
				echo json_encode($x);
			} else {
				$test['message'] = "Failed!";
				$x[] = $test;
				header('Content-Type: application/json');
				echo json_encode($x);
			}
		} else {
			$test['message'] = "Invalid User ID!";
			$x[] = $test;
			header('Content-Type: application/json');
			echo json_encode($x);
		}
	}

	function editTask()
	{
		$customer_id = $this->input->post('customer_id');
		$user_id = $this->input->post('user_id');
		$followup_id = $this->input->post('followup_id');

		if (isset($customer_id) && $customer_id != NULL && isset($user_id) && $user_id != NULL && isset($followup_id) && $followup_id != NULL) {
			$this->db->where('followup_id', $followup_id);
			$this->db->where('status', 1);
			$query = $this->db->get('tta_followup');
			if ($query->num_rows() == 1) {
				$data["followup_for"] = $this->input->post("followup_for");
				$data["followup_date"] = date('Y-m-d', strtotime($this->input->post("followup_date")));
				$data["followup_from_time"] = date('H:i:s', strtotime($this->input->post("followup_from_time")));
				$data["followup_to_time"] = date('H:s', strtotime($this->input->post("followup_to_time")));
				$data["followup_title"] = $this->input->post("followup_title");
				$data["followup_description"] = $this->input->post("followup_description");
				$data["followup_priority"] = $this->input->post("followup_priority");
				$data["followup_status"] = $this->input->post("followup_status");
				$data["update_date_time"] = date("Y-m-d H:m:i");
				$data["updated_by"] = $user_id;

				$this->db->where('followup_id', $followup_id);
				$this->db->where('customer_id', $customer_id);
				$this->db->where('status', 1);
				if ($this->db->update("tta_followup", $data)) {
					$test['message'] = "Updated Successfully.";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				} else {
					$test['message'] = "Failed!";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				}
			} else {
				$test['message'] = "Invalid User!";
				$x[] = $test;
				header('Content-Type: application/json');
				echo json_encode($x);
			}
		} else {
			$test['message'] = "Invalid User ID!";
			$x[] = $test;
			header('Content-Type: application/json');
			echo json_encode($x);
		}
	}

	function deleteTask()
	{
		$customer_id = $this->input->post('customer_id');
		$user_id = $this->input->post('user_id');
		$followup_id = $this->input->post('followup_id');

		if (isset($customer_id) && $customer_id != NULL && isset($user_id) && $user_id != NULL && isset($followup_id) && $followup_id != NULL) {
			$this->db->where('followup_id', $followup_id);
			$this->db->where('status', 1);
			$query = $this->db->get('tta_followup');
			if ($query->num_rows() == 1) {
				$data["status"] = '-1';
				$data["update_date_time"] = date("Y-m-d H:m:i");
				$data["updated_by"] = $user_id;

				$this->db->where('followup_id', $followup_id);
				$this->db->where('customer_id', $customer_id);
				$this->db->where('status', 1);
				if ($this->db->update("tta_followup", $data)) {
					$test['message'] = "Deleted Successfully.";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				} else {
					$test['message'] = "Failed!";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				}
			} else {
				$test['message'] = "Invalid User!";
				$x[] = $test;
				header('Content-Type: application/json');
				echo json_encode($x);
			}
		} else {
			$test['message'] = "Invalid User ID!";
			$x[] = $test;
			header('Content-Type: application/json');
			echo json_encode($x);
		}
	}

	function changeFollowupStatus($user_id = '', $followup_id = '', $followup_status = '')
	{
		if (isset($user_id) && $user_id != NULL && isset($followup_id) && $followup_id != NULL && isset($followup_status) && $followup_status != NULL) {
			$this->db->where('followup_id', $followup_id);
			$this->db->where('status', 1);
			$query = $this->db->get('tta_followup');
			if ($query->num_rows() == 1) {
				$data['followup_status'] = $followup_status;
				$this->db->where('followup_id', $followup_id);
				$this->db->where('followup_for', $user_id);
				$this->db->where('status', 1);
				if ($this->db->update('tta_followup', $data)) {
					$test['message'] = "Updated Successfully.";
					$x[] = $test;
					header('Content-Type: application/json');
					echo json_encode($x);
				} else {
					$test['message'] = "Update failed!";
					header('Content-Type: application/json');
					echo json_encode($test);
				}
			} else {
				$test['message'] = "Invalid User!";
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$test['message'] = "Invalid User ID!";
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}
}
