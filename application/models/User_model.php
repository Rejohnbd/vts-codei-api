<?php class User_model extends CI_Model
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function changePassword()
	{
		$response = '';
		$user_id = $this->input->post('user_id');
		$login_email = $this->input->post('login_email');
		$current_password = $this->input->post('current_password');
		$new_password = $this->input->post('new_password');
		if (isset($user_id) && $user_id != NULL) {
			$this->db->where('user_login_email', $login_email);
			$this->db->where('user_password', hash('sha512', $current_password));
			$this->db->where('user_status', 1);
			$query = $this->db->get('tta_users');
			if ($query->num_rows() == 1) {
				$dataForUpdate['user_password'] = hash('sha512', $new_password);
				$this->db->where('user_id', $user_id);
				$this->db->where('user_login_email', $login_email);
				if ($this->db->update('tta_users', $dataForUpdate)) {
					$response['message'] = "Password updated successfully.";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$response['message'] = "Update Failed!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				}
			} else {
				$response['message'] = "Wrong Password.";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$response['message'] = "Invalid User!";
			$test[] = $response;
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}
}
