<?php class Gps_model extends CI_Model
{

	function addData()
	{

		$imei = $this->input->post('imei');
		if (isset($imei) && $imei != NULL) {
			$deviceInfo = $this->getDeviceInfoByImei($imei);
			if (isset($deviceInfo) && $deviceInfo != NULL) {
				$data['device_id'] = $deviceInfo['device_id'];
				$data['device_imei'] = $deviceInfo['device_imei'];
				$data['device_time'] = date('Y-m-d H:i:s', strtotime($this->input->post('device_time')));
				$data['lat'] = $this->input->post('lat');
				$data['lng'] = $this->input->post('lng');
				$data['speed'] = $this->input->post('speed');
				$data['course'] = $this->input->post('course');
				$distance = $this->getDistance($deviceInfo['device_id'], $data['lat'], $data['lng']);
				$data['distance'] = $distance;
				$data['engine_status'] = $this->input->post('status');
				$data['address'] = $this->getAddress($this->input->post('lat'), $this->input->post('lng'));
				$data['attributes'] = $this->input->post('attributes');
				if (isset($data['device_id']) && $data['device_id'] != NULL && isset($data['device_imei']) && $data['device_imei'] != NULL && isset($data['lat']) && $data['lat'] != NULL && isset($data['lng']) && $data['lng'] != NULL && isset($data['speed']) && $data['speed'] != NULL && isset($data['engine_status']) && $data['engine_status'] != NULL) {
					$this->db->insert('tta_position', $data);
					$id = $this->db->insert_id();
					if (isset($id) && $id != NULL) {
						echo 'Data Received.';
						// Call alert function here.
					} else {
						echo 'Failed!';
					}
				} else {
					echo 'missing parm';
				}
			} else {
				echo 'Invalid Device!';
			}
		} else {
			echo 'Invalid Request!';
		}
	}

	function getDistance($device_id = "", $latitudeTo = '', $longitudeTo = '')
	{
		if (isset($device_id) && $device_id != NULL) {
			$this->db->where('device_id', $device_id);
			$this->db->order_by('position_id', 'desc');
			$this->db->limit(1);
			$query = $this->db->get('tta_position');
			if ($query->num_rows() == 1) {
				foreach ($query->result() as $row) {
					$latitudeFrom = $row->lat;
					$longitudeFrom = $row->lng;
				}
				$distance = $this->distanceCalculation($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo);
				return $distance;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}

	function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2)
	{
		// Calculate the distance in degrees
		$degrees = rad2deg(acos((sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($point1_long - $point2_long)))));

		// Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
		switch ($unit) {
			case 'km':
				$distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
				break;
			case 'mi':
				$distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
				break;
			case 'nmi':
				$distance =  $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
		}
		return round($distance, $decimals);
	}

	function getAddress($lat = "", $long = "")
	{
		if (isset($lat) && $lat != NULL && isset($long) && $long != NULL) {
			// api url http://103.248.13.78/nominatim/reverse.php?format=json&lat=24.112855879370198&lon=92.00790166854858
			// dhanmondi 23.750538, 90.367716
			//$lat = '23.750538';
			//$long = '90.367716';
			// $url = 'http://103.248.13.78/nominatim/reverse.php?format=json&lat=' . $lat . '&lon=' . $long;
			// $content = json_decode($this->url_get($url));
			// var_dump($content);
			// $display_name = $content->{'display_name'};
			//$display_name = 'D';
			// Added by Rejohn
			$opts = array(
				'http' => array(
					'method' => "GET",
					'header' => "User-Agent: lashaparesha api script\r\n"
				)
			);

			$context = stream_context_create($opts);

			$url = 'https://nominatim.openstreetmap.org/reverse?format=json&lat=' . $lat . '&lon=' . $long . '&zoom=27&addressdetails=1';

			$jsonData = file_get_contents($url, false, $context);
			$data = json_decode($jsonData);
			$display_name = $data->display_name;

			if (isset($display_name) && $display_name != NULL) {
				return $display_name;
			} else {
				$x = '';
				return $x;
			}
			//var_dump($address->{'village'}.$address->{'county'});
		} else {
			$y = '';
			return $y;
		}
	}

	function url_get($url)
	{
		$ch = curl_init();    // initialize curl handle
		curl_setopt($ch, CURLOPT_URL, $url); // set url to post 
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); // times out after 10s
		$urlcontent = curl_exec($ch);
		curl_close($ch);
		return ($urlcontent);
	}

	function getDeviceInfoByImei($imei = "")
	{
		$this->db->from('tta_devices');
		$this->db->where('device_imei', $imei);
		$this->db->where('device_status', 1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$result['device_id'] = $row->device_id;
				$result['device_type'] = $row->device_type;
				$result['vehicle_id'] = $row->vehicle_id;
				$result['customer_id'] = $row->customer_id;
				$result['device_imei'] = $row->device_imei;
				$result['device_unique_id'] = $row->device_unique_id;
				$result['device_activation_status'] = $row->device_activation_status;
				$result['device_activated_by'] = $row->device_activated_by;
				$result['device_activated_date_time'] = $row->device_activated_date_time;
				$result['device_deactivated_by'] = $row->device_deactivated_by;
				$result['device_deactivated_date_time'] = $row->device_deactivated_date_time;
				$result['device_authorization_code'] = $row->device_authorization_code;
				$result['device_authorizad_by'] = $row->device_authorizad_by;
				$result['device_authorized_date_time'] = $row->device_authorized_date_time;
				$result['device_deauthorized_by'] = $row->device_deauthorized_by;
				$result['device_deauthorized_date_time'] = $row->device_deauthorized_date_time;
				$result['device_asigned_status'] = $row->device_asigned_status;
				$result['device_asigned_to'] = $row->device_asigned_to;
				$result['device_asigned_date'] = $row->device_asigned_date;
				$result['device_sim_number'] = $row->device_sim_number;
				$result['device_current_url'] = $row->device_current_url;
				$result['device_old_url'] = $row->device_old_url;
				$result['device_update_url'] = $row->device_update_url;
				$result['device_varified'] = $row->device_varified;
				$result['device_firmware'] = $row->device_firmware;
				$result['device_version'] = $row->device_version;
				$result['device_sleep_interval'] = $row->device_sleep_interval;
				$result['device_note'] = $row->device_note;
				$result['device_status'] = $row->device_status;
				$result['device_inserted_by'] = $row->device_inserted_by;
				$result['device_insert_date_time'] = $row->device_insert_date_time;
				$result['device_updated_by'] = $row->device_updated_by;
				$result['device_update_date_time'] = $row->device_update_date_time;
			}
			return $result;
		} else {
			return false;
		}
	}
	/**
	 * Added by Rejohn
	 * for Seeworld device data test
	 */

	function add_vts_temp_data()
	{
		$imei = $this->input->post('imei');
		if (isset($imei) && $imei != NULL) {
			// $deviceInfo = $this->getDeviceInfoByImei($imei);

			$data['device_id'] = $this->input->post('imei');
			$data['device_imei'] = $this->input->post('imei');
			$data['device_time'] = date('Y-m-d H:i:s', strtotime($this->input->post('device_time')));
			$data['lat'] = $this->input->post('lat');
			$data['lng'] = $this->input->post('lng');
			$data['speed'] = $this->input->post('speed');
			$data['course'] = $this->input->post('course');
			// $distance = $this->getDistance($deviceInfo['device_id'], $data['lat'], $data['lng']);
			$data['distance'] = 0;
			$data['engine_status'] = $this->input->post('status');
			$data['temperature'] = $this->input->post('temperature');
			$data['address'] = $this->getAddress($this->input->post('lat'), $this->input->post('lng'));
			$data['attributes'] = $this->input->post('attributes');
			if (isset($data['device_id']) && $data['device_id'] != NULL && isset($data['device_imei']) && $data['device_imei'] != NULL && isset($data['lat']) && $data['lat'] != NULL && isset($data['lng']) && $data['lng'] != NULL && isset($data['speed']) && $data['speed'] != NULL && isset($data['engine_status']) && $data['engine_status'] != NULL) {
				$this->db->insert('tta_position_with_temp', $data);
				$id = $this->db->insert_id();
				if (isset($id) && $id != NULL) {
					echo 'Data Received for Temp.';
					// Call alert function here.
				} else {
					echo 'Failed!';
				}
			} else {
				echo 'missing parm';
			}
		} else {
			echo 'Invalid Request!';
		}
	}
}
