<?php class Vehicle_model extends CI_Model
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}




	function getLastCoordinate()
	{
		$response = '';
		$device_imei = $this->input->post('device_imei');
		if (isset($device_imei) && $device_imei != NULL) {
			$deviceInfo = $this->getDeviceInfoByImei($device_imei);
			$returnedResult = $this->getLastCoordinatesByDeviceId($deviceInfo['device_id']);
			if (isset($returnedResult) && $returnedResult != NULL) {
				$response['success'] = 1;
				foreach ($returnedResult as $Coordinance) {
					$response['lat'] = $Coordinance->lat;
					$response['lng'] = $Coordinance->lng;
					$response['speed'] = $Coordinance->speed;
				}
				$response['message'] = 'Last Coordinate.';
			} else {
				$response[] = array(
					'success' => 0,
					'vehicles' => NULL,
					'message' => 'Invalid IMEI ID!'
				);
			}
		} else {
			$response[] = array(
				'success' => 0,
				'vehicles' => NULL,
				'message' => 'Invalid Request!'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}


	function getCoordinaneByPositionID($position_id = "")
	{
		$this->db - where('position_id', $position_id);
		$query = $this->db->get('tta_position');
		if ($query->num_rows() > 0) {
		}
	}

	function getLastCoordinatesByDeviceId($device_id = "")
	{
		$this->db->where('device_id', $device_id);
		$this->db->order_by("position_id", "desc");
		$this->db->limit(1);
		$query1 = $this->db->get('tta_position');
		if ($query1->num_rows() == 1) {
			return $query1->result();
		}
	}

	function getLast3Coordinance($device_imei = '')
	{
		$this->db->where('device_imei', $device_imei);
		$query = $this->db->get('tta_devices');
		foreach ($query->result() as $row) {
			$device_id = $row->device_id;
		}

		$this->db->where('device_id', $device_id);
		$this->db->order_by("position_id", "desc");
		$this->db->limit(3);
		$query1 = $this->db->get('tta_position');
		if ($query1->num_rows() == 3) {
			$result = $query1->result();
			return $result;
		}
	}


	function getVehicles()
	{
		$response = '';
		$customer_id = $this->input->post('customer_id');
		if (isset($customer_id) && $customer_id != NULL) {
			$returnedResult = $this->getAllVehiclesByCustomerId($customer_id);
			if (isset($returnedResult) && $returnedResult != NULL) {
				$response['success'] = 1;
				$response['totalVehicles'] = sizeof($returnedResult);
				$count = 1;
				foreach ($returnedResult as $vehicle) {
					$vehicleList['vechile' . $count] = $this->getVehicleInfoByVehicleID($vehicle->vehicle_id);
					$count++;
				}
				$response['vehicles'] = $vehicleList;
				$response['message'] = 'All Customer List.';
			} else {
				$response[] = array(
					'success' => 0,
					'vehicles' => NULL,
					'message' => 'Invalid Customer ID!'
				);
			}
		} else {
			$response[] = array(
				'success' => 0,
				'vehicles' => NULL,
				'message' => 'Invalid Request!'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	function getVehicleInfoByVehicleID($vehicle_id = '')
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$query = $this->db->get('tta_vehicles');
		if ($query->num_rows() == 1) {
			foreach ($query->result() as $row) {
				$result['vehicle_number'] = $row->vehicle_number;
				$result['vehicle_year_make_model'] = $row->vehicle_year_make_model;
				$result['vehicle_description'] = $row->vehicle_description;
				$result['vehicle_max_speed'] = $row->vehicle_max_speed;
				$result['vehicle_driver_name'] = $row->vehicle_driver_name;
				$result['vehicle_driver_phone'] = $row->vehicle_driver_phone;
			}
			return $result;
		} else {
			return false;
		}
	}

	/**
	 * Added by Rejohn
	 * for web api
	 */

	function getAllVehiclesByCustomerId($customer_id = '')
	{
		$this->db->where('customer_id', $customer_id);
		$this->db->where('vehicle_status', 1);
		$this->db->order_by('tta_vehicles.vehicle_year_make_model', 'asc');
		$query = $this->db->get('tta_vehicles');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function get_all_vehicle_types()
	{
		$query_result = $this->db->get('tta_vehicle_types');
		return $query_result->result();
	}

	public function get_all_vehicle_colors()
	{
		$query_result = $this->db->get('tta_vehicle_color');
		return $query_result->result();
	}

	public function add_vehicle_to_customer($data)
	{
		$createVehicle = array(
			'customer_id'						=> $data['customer_id'],
			'vehicle_number'					=> $data['vehicle_number'],
			'vehicle_max_speed'					=> $data['vehicle_max_speed'],
			'vehicle_type_id'					=> $data['vehicle_type_id'],
			'vehicle_color_id'					=> $data['vehicle_color_id'],
			'vehicle_year_make_model'			=> $data['vehicle_year_make_model'],
			'vehicle_description'				=> $data['vehicle_description'],
			'vehicle_device_cost'				=> $data['vehicle_device_cost'],
			'vehicle_monthly_recurring_charge'	=> $data['vehicle_monthly_recurring_charge'],
			'vehicle_distence_per_unit'			=> $data['vehicle_distence_per_unit'],
			'vehicle_driver_name'				=> $data['vehicle_driver_name'],
			'vehicle_fathers_name'				=> $data['vehicle_fathers_name'],
			'vehicle_driver_phone'				=> $data['vehicle_driver_phone'],
			'vehicle_driver_dob'				=> date('Y-m-d', strtotime($data['vehicle_driver_dob'])),
			'vehicle_driver_license'			=> $data['vehicle_driver_license'],
			'vehicle_address'					=> $data['vehicle_address'],
			'vehicle_inserted_by'				=> $data['vehicle_inserted_by'],
			'vehicle_insert_date_time'			=> date("Y-m-d H:m:i"),
			'vehicle_status'					=> 1,
		);

		$this->db->insert('tta_vehicles', $createVehicle);
	}
}
