<?php class Device_model extends CI_Model
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}


	// Old Function
	function getDeviceInfoByImei($device_imei = "")
	{
		$this->db->where('device_imei', $device_imei);
		$this->db->where('device_status', 1);
		$query = $this->db->get('tta_devices');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$result['device_id'] = $row->device_id;
				$result['device_type'] = $row->device_type;
				$result['vehicle_id'] = $row->vehicle_id;
				$result['customer_id'] = $row->customer_id;
				$result['device_imei'] = $row->device_imei;
				$result['device_unique_id'] = $row->device_unique_id;
				$result['device_activation_status'] = $row->device_activation_status;
				$result['device_activated_by'] = $row->device_activated_by;
				$result['device_activated_date_time'] = $row->device_activated_date_time;
				$result['device_deactivated_by'] = $row->device_deactivated_by;
				$result['device_deactivated_date_time'] = $row->device_deactivated_date_time;
				$result['device_authorization_code'] = $row->device_authorization_code;
				$result['device_authorizad_by'] = $row->device_authorizad_by;
				$result['device_authorized_date_time'] = $row->device_authorized_date_time;
				$result['device_deauthorized_by'] = $row->device_deauthorized_by;
				$result['device_deauthorized_date_time'] = $row->device_deauthorized_date_time;
				$result['device_asigned_status'] = $row->device_asigned_status;
				$result['device_asigned_to'] = $row->device_asigned_to;
				$result['device_asigned_date'] = $row->device_asigned_date;
				$result['device_sim_number'] = $row->device_sim_number;
				$result['device_current_url'] = $row->device_current_url;
				$result['device_old_url'] = $row->device_old_url;
				$result['device_update_url'] = $row->device_update_url;
				$result['device_varified'] = $row->device_varified;
				$result['device_firmware'] = $row->device_firmware;
				$result['device_version'] = $row->device_version;
				$result['device_sleep_interval'] = $row->device_sleep_interval;
				$result['device_note'] = $row->device_note;
				$result['device_status'] = $row->device_status;
				$result['device_inserted_by'] = $row->device_inserted_by;
				$result['device_insert_date_time'] = $row->device_insert_date_time;
				$result['device_updated_by'] = $row->device_updated_by;
				$result['device_update_date_time'] = $row->device_update_date_time;
			}
			return $result;
		} else {
			return false;
		}
	}


	function inquiryDataByDeviceId()
	{
		$device_id = $this->input->post('device_id');

		$this->db->where('device_id', $device_id);
		$query = $this->db->get('tta_position');
		if ($query->num_rows() > 0) {
			$this->load->dbutil();
			echo $this->dbutil->csv_from_result($query);
		} else {
			$response['Total Data'] = '0';
			$test[] = $response;
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}

	function downloadDataByDeviceId()
	{
		$device_id = $this->input->post('device_id');

		$this->db->where('device_id', $device_id);
		$query = $this->db->get('tta_position');
		if ($query->num_rows() > 0) {
			$this->load->dbutil();
			$this->load->helper('file');
			$this->load->helper('download');
			$delimiter = ",";
			$newline = "\r\n";
			$data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
			force_download('CSV_Report.csv', $data);
		} else {
			$response['Total Data'] = '0';
			$test[] = $response;
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}

	/**
	 * Added by Rejohn
	 * for web api
	 */
	public function number_of_all_devicess()
	{
		$query_result = $this->db->get('tta_devices');
		return $query_result->num_rows();
	}

	public function number_of_active_devices()
	{
		$this->db->from('tta_devices');
		$this->db->where('device_status', 1);
		$query_result = $this->db->get();
		return $query_result->num_rows();
	}

	public function add_device($data)
	{
		$deviceData = array(
			'device_type'				=> $data['device_type'],
			'device_imei'				=> $data['device_imei'],
			'device_unique_id'			=> $data['device_unique_id'],
			'device_activation_status'	=> 0,
			'device_asigned_status'		=> $data['device_asigned_status'],
			'device_sim_type'			=> $data['device_sim_type'],
			'device_sim_number'			=> $data['device_sim_number'],
			'device_recharge_amount' 	=> $data['device_recharge_amount'],
			'device_note'				=> $data['device_note'],
			'device_inserted_by' 		=> $data['device_inserted_by'],
			'device_insert_date_time' 	=> date("Y-m-d H:m:i"),
			'device_status' 			=> 0,
		);
		$this->db->insert('tta_devices', $deviceData);
	}

	public function get_all_devices()
	{
		$results = array();
		$this->db->select('device_id, customer_id, device_imei, device_sim_number, device_asigned_status, device_recharge_amount');
		$query_result = $this->db->get('tta_devices');
		$devices = $query_result->result();

		foreach ($devices as $key => $device) {
			if ($device->customer_id != NULL) {
				$this->db->select('customer_id, customer_name');
				$this->db->from('tta_customers');
				$this->db->where('customer_id', $device->customer_id);
				$query_result = $this->db->get();
				$customerInfo = $query_result->row();

				$results[$key]['device_id']					= $device->device_id;
				$results[$key]['device_imei']				= $device->device_imei;
				$results[$key]['device_sim_number']			= $device->device_sim_number;
				$results[$key]['device_asigned_status']		= $device->device_asigned_status;
				$results[$key]['device_recharge_amount']	= $device->device_recharge_amount;
				$results[$key]['customer_id']				= $customerInfo->customer_id;
				$results[$key]['customer_name']				= $customerInfo->customer_name;
			} else {
				$results[$key]['device_id']					= $device->device_id;
				$results[$key]['device_imei']				= $device->device_imei;
				$results[$key]['device_sim_number']			= $device->device_sim_number;
				$results[$key]['device_asigned_status']		= $device->device_asigned_status;
				$results[$key]['device_recharge_amount']	= $device->device_recharge_amount;
				$results[$key]['customer_id']				= "";
				$results[$key]['customer_name']				= "";
			}
		}
		return $results;
	}
}
