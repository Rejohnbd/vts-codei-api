<?php class Fake_model extends CI_Model
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function addFakeData()
	{
		$data['device_id'] = $this->input->post('device_id');
		$data['device_imei'] = $this->input->post('device_imei');
		$data['server_time'] = date('Y-m-d H:i:s', strtotime($this->input->post('server_time')));
		$data['device_time'] = date('Y-m-d H:i:s', strtotime($this->input->post('device_time')));
		$data['lat'] = $this->input->post('lat');
		$data['lng'] = $this->input->post('lng');
		$data['speed'] = $this->input->post('speed');
		$data['distance'] = $this->input->post('distance');
		$data['engine_status'] = $this->input->post('engine_status');
		$data['course'] = $this->input->post('course');
		$data['address'] = $this->input->post('address');
		$data['attributes'] = $this->input->post('attributes');
		if ($this->db->insert('tta_position_demo', $data)) {
			$response['message'] = 'Inserted Successfully.';
		} else {
			$response['message'] = 'Failed!';
		}
		$test[] = $response;
		header('Content-Type: application/json');
		echo json_encode($test);
	}

	function getFakeData()
	{
		$this->db->where('device_id', $this->input->post("device_id"));
		$this->db->order_by("position_id", "desc");
		$this->db->limit(2);
		$query1 = $this->db->get('tta_position_demo');
		if ($query1->num_rows() > 1) {
			$count = 1;
			foreach ($query1->result() as $row) {
				if ($count == 1) {
					$customOutput["position_id"] = $row->position_id;
					$customOutput["device_id"] = $row->device_id;
					$customOutput["device_imei"] = $row->device_imei;
					$customOutput["server_time"] = $row->server_time;
					$customOutput["device_time"] = $row->device_time;
					$customOutput["lat"] = $row->lat;
					$customOutput["lng"] = $row->lng;
					$customOutput["distance"] = $row->distance;
					$customOutput["speed"] = $row->speed;
					$customOutput["engine_status"] = $row->engine_status;
					$customOutput["course"] = $row->course;
					$customOutput["address"] = $row->address;
					$customOutput["attributes"] = $row->attributes;
					$customOutput['message'] = "Successful.";
				} else {
					$customOutput["last_lat"] = $row->lat;
					$customOutput["last_lng"] = $row->lng;
				}
				$count++;
			}

			$test[] = $customOutput;
			header('Content-Type: application/json');
			echo json_encode($test);
		} else {
			$response['message'] = "No Data Found!";
			$test[] = $response;
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}
}
