<?php class Customer_model extends CI_Model
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getProfile($customer_id = '', $user_id = '')
	{
		if (isset($customer_id) && $customer_id != NULL && isset($user_id) && $user_id != NULL) {
			$this->db->where('customer_id', $customer_id);
			$query = $this->db->get('tta_customers');
			if ($query->num_rows() == 1) {
				foreach ($query->result() as $row) {
					$result['customer_name'] = $row->customer_name;
					$result['customer_primary_email'] = $row->customer_primary_email;
					$result['customer_billing_email'] = $row->customer_billing_email;
					$result['customer_primary_phone'] = $row->customer_primary_phone;
					$result['customer_secondary_phone'] = $row->customer_secondary_phone;
					$result['customer_address'] = $row->customer_address;
				}
				$test['customer_info'] = $result;

				$this->db->where('customer_id', $customer_id);
				$this->db->where('user_id', $user_id);
				$query1 = $this->db->get('tta_users');
				if ($query1->num_rows() == 1) {
					foreach ($query1->result() as $row1) {
						$result1['user_name'] = $row1->user_name;
						$result1['primary_phone'] = $row1->primary_phone;
						$result1['cell_phone'] = $row1->cell_phone;
					}
				} else {
					$result1['user_info'] = "Invalid User!";
				}

				$test['user_info'] = $result1;

				$test['message'] = "Customer Information";

				$x[] = $test;
				header('Content-Type: application/json');
				echo json_encode($x);
			} else {
				$test['message'] = "No Customer Found!";
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$test['message'] = "Invalid Customer ID!";
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}

	function updateProfile($customer_id = '', $user_id = '', $vehicle_id = '')
	{
		if (isset($customer_id) && $customer_id != NULL && isset($user_id) && $user_id != NULL && isset($vehicle_id) && $vehicle_id != NULL) {
			$this->db->where('customer_id', $customer_id);
			$this->db->where('vehicle_id', $vehicle_id);
			$this->db->where('vehicle_status', 1);
			$query = $this->db->get('tta_vehicles');
			if ($query->num_rows() == 1) {
				$data['vehicle_max_speed'] = $this->input->post('vehicle_max_speed');
				$data['vehicle_driver_name'] = $this->input->post('vehicle_driver_name');
				$data['vehicle_fathers_name'] = $this->input->post('vehicle_fathers_name');
				$data['vehicle_driver_license'] = $this->input->post('vehicle_driver_license');
				$data['vehicle_address'] = $this->input->post('vehicle_address');
				$data['vehicle_driver_phone'] = $this->input->post('vehicle_driver_phone');
				//$data ['vehicle_driver_dob'] = date('Y-m-d', strtotime($this->input->post('vehicle_driver_dob'))); 
				$data['vehicle_updated_by'] = $user_id;
				$data['vehicle_update_date_time'] = date('Y-m-d H:i:s');
				$data['vehicle_distence_per_unit'] = $this->input->post('vehicle_distence_per_unit');

				$this->db->where('vehicle_id', $vehicle_id);
				$this->db->where('customer_id', $customer_id); // for hacking protection
				if ($this->db->update('tta_vehicles', $data)) {
					$test['message'] = "Updated Successfully.";
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$test['message'] = "Updated Failed!";
					header('Content-Type: application/json');
					echo json_encode($test);
				}
			} else {
				$test['message'] = "Invalid Vehicle ID!";
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$test['message'] = "Invalid Customer ID!";
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}

	/**
	 * Added by Rejohn
	 * for web api
	 */

	public function number_of_all_customers()
	{
		$query_result = $this->db->get('tta_customers');
		return $query_result->num_rows();
	}

	public function number_of_active_customers()
	{
		$this->db->from('tta_customers');
		$this->db->where('customer_status', 1);
		$query_result = $this->db->get();
		return $query_result->num_rows();
	}

	public function all_customers_with_vehicle()
	{
		$results = array();
		$this->db->select('customer_id, customer_name, customer_primary_email, customer_primary_phone, customer_secondary_phone, customer_address, customer_signup_date_time');
		$query = $this->db->get('tta_customers');
		$customers = $query->result();
		foreach ($customers as $key => $customer) {
			$this->db->from('tta_vehicles');
			$this->db->where('customer_id', $customer->customer_id);
			$query = $this->db->get();

			$results[$key]['customer_id']					= $customer->customer_id;
			$results[$key]['customer_name']				= $customer->customer_name;
			$results[$key]['customer_primary_email']		= $customer->customer_primary_email;
			$results[$key]['customer_primary_phone']		= $customer->customer_primary_phone;
			$results[$key]['customer_secondary_phone']	= $customer->customer_secondary_phone;
			$results[$key]['customer_address']			= $customer->customer_address;
			$results[$key]['customer_signup_date_time']	= $customer->customer_signup_date_time;
			$results[$key]['vehicle_numbers']				= $query->num_rows();
		}
		return $results;
	}

	public function creae_customer($data)
	{
		$custData = array(
			'customer_type_id' 				=> $data['customer_type_id'],
			'customer_name' 				=> $data['customer_name'],
			'customer_primary_email' 		=> $data['customer_primary_email'],
			'customer_billing_email' 		=> $data['customer_billing_email'],
			'customer_alert_email' 			=> $data['customer_alert_email'],
			'customer_primary_phone' 		=> $data['customer_primary_phone'],
			'customer_secondary_phone' 		=> $data['customer_secondary_phone'],
			'customer_address' 				=> $data['customer_address'],
			'customer_billing_date' 		=> $data['customer_billing_date'],
			'customer_cost_per_device' 		=> $data['customer_cost_per_device'],
			'customer_cost_per_vehicle' 	=> $data['customer_cost_per_vehicle'],
			'customer_activated_by' 		=> $data['customer_activated_by'],
			'customer_inserted_by' 			=> $data['customer_inserted_by'],
			'customer_notes' 				=> $data['customer_notes'],
			'customer_remarks' 				=> $data['customer_remarks'],
			'customer_sms' 					=> $data['customer_sms'],
			'customer_rederred_by' 			=> $data['customer_rederred_by'],
			'customer_signup_date_time'		=> date("Y-m-d H:m:i"),
			'customer_insert_date_time'		=> date("Y-m-d H:m:i"),
			'customer_activated_date_time' 	=> date("Y-m-d H:m:i"),
			'customer_status' 				=> 1
		);
		$this->db->insert('tta_customers', $custData);
		$insertId = $this->db->insert_id();

		$userData = array(
			'user_type_id'		=> 101,
			'customer_id'		=> $insertId,
			'user_login_email'	=> $data['customer_primary_email'],
			'user_password'		=> hash('sha512', $data['user_password']),
			'user_name'			=> $data['customer_name'],
			'primary_phone'		=> $data['customer_primary_phone'],
			'insert_date_time'	=> date("Y-m-d H:m:i"),
			'inserted_by'		=> wi$data['customer_inserted_by'],
			'user_status'		=> 1,
		);
		$this->db->insert('tta_users', $userData);
	}

	public function get_customer_details_by_id($customer_id)
	{
		$this->db->select('customer_id, customer_name, customer_primary_email, customer_primary_phone, customer_secondary_phone, customer_address, customer_signup_date_time, customer_billing_date, customer_sms, customer_cost_per_device, customer_cost_per_vehicle');
		$this->db->where('customer_id', $customer_id);
		$query_result = $this->db->get('tta_customers');
		return $query_result->row();
	}

	public function get_user_info_by_customer_id($customer_id)
	{
		$this->db->select('user_name, user_login_email, primary_phone, cell_phone');
		$this->db->where('customer_id', $customer_id);
		$query_result = $this->db->get('tta_users');
		return $query_result->row();
	}

	public function get_vehicle_info_by_customer_id($customer_id)
	{
		$this->db->select('vehicle_number, vehicle_year_make_model, vehicle_description, vehicle_driver_name, vehicle_insert_date_time, vehicle_monthly_recurring_charge');
		$this->db->where('customer_id', $customer_id);
		$query_result = $this->db->get('tta_vehicles');
		return $query_result->result();
	}
}
