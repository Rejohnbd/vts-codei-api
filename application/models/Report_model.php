<?php class Report_model extends CI_Model
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getMonthlyReport()
	{
		$response = '';
		$device_imei = $this->input->post('device_imei');
		$from_date = $this->input->post('from_date');
		$to_date = $this->input->post('to_date');

		if (isset($device_imei) && $device_imei != NULL && isset($from_date) && $from_date != NULL && isset($to_date) && $to_date != NULL) {
			$returnedDeviceInfo = $this->device_model->getDeviceInfoByImei($device_imei);
			if (isset($returnedDeviceInfo) && $returnedDeviceInfo != NULL) {
				$response = array(
					'success' => 1,
					'coordinates' => $this->getCoordinatesByDateRange($from_date, $to_date),
					'message' => 'Monthly Report',
					'total' => count($returnedDates)
				);

				header('Content-Type: application/json');
				echo json_encode($response);
			} else {
				echo 'Invalid Device!';
			}
		}
	}

	function get24hrsSummeryReport()
	{
		$response = '';
		$device_id = $this->input->post('device_id');
		$date = $this->input->post('date');
		$vehicle_distence_per_unit = $this->input->post('vehicle_distence_per_unit');
		$today = date('Y-m-d', strtotime($date));
		if (isset($device_id) && $device_id != NULL) {
			//$deviceInfo = $this->getDeviceInfoByImei($device_imei);
			//print_r($deviceInfo['device_id']);
			//$vehicleInfo = $this->getVehicleInfoById($deviceInfo['vehicle_id']);


			$hour = 24;
			$totalDistanceAdded = 0;
			$totalFuelAdded = 0;

			for ($x = 1; $x <= $hour; $x++) {
				if ($x == 1) {
					$timeText = 'st';
				} elseif ($x == 2) {
					$timeText = 'nd';
				} elseif ($x == 3) {
					$timeText = 'rd';
				} elseif ($x >= 4 && $x <= 20) {
					$timeText = 'th';
				} elseif ($x == 21) {
					$timeText = 'st';
				} elseif ($x == 22) {
					$timeText = 'nd';
				} elseif ($x == 23) {
					$timeText = 'rd';
				}

				$fromX = $x - 1;
				$newFrom = $fromX . ':00:00';
				$from = date('h:s A', strtotime($newFrom));

				$toX = $x;
				$newTo = $toX . ':00:00';
				$to = date('h:s A', strtotime($newTo));




				$returnData = $this->getHourlyReport($device_id, $today . ' ' . $newFrom, $today . ' ' . $newTo);
				if (isset($returnData['distance'])) {
					$distance = $returnData['distance'];
					$fuel = $returnData['distance'] / $vehicle_distence_per_unit;
					$address = $returnData['address'];
				} else {
					$distance = 0;
					$fuel = 0;
					$address = "";
				}



				$addressPart = explode(',', $address);


				$totalDistanceAdded += $distance;
				$totalFuelAdded += $fuel;
				$singleLine['from'] = $from;
				$singleLine['to'] = $to;
				$singleLine['address'] = $addressPart[0];
				$singleLine['distance'] = $distance;
				$singleLine['fuel'] = $fuel;

				$dataX[] = $singleLine;

				$addressPart[0] = "";
				$returnData['distance'] = "";
			}
			header('Content-Type: application/json');
			echo json_encode($dataX);
		}
	}


	function getVehicleInfoById($vehicle_id = "")
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$query = $this->db->get('tta_vehicles');
		if ($query->num_rows() == 1) {
			foreach ($query->result() as $row) {
				$result['vehicle_id'] = $row->vehicle_id;
				$result['customer_id'] = $row->customer_id;
				$result['device_id'] = $row->device_id;
				$result['vehicle_type_id'] = $row->vehicle_type_id;
				$result['vehicle_color_id'] = $row->vehicle_color_id;
				$result['vehicle_device_status'] = $row->vehicle_device_status;
				$result['vehicle_device_assigned_by'] = $row->vehicle_device_assigned_by;
				$result['vehicle_device_assign_date_time'] = $row->vehicle_device_assign_date_time;
				$result['vehicle_device_unassigned_by'] = $row->vehicle_device_unassigned_by;
				$result['vehicle_device_unassign_date_time'] = $row->vehicle_device_unassign_date_time;
				$result['vehicle_device_cost'] = $row->vehicle_device_cost;
				$result['vehicle_monthly_recurring_charge'] = $row->vehicle_monthly_recurring_charge;
				$result['vehicle_number'] = $row->vehicle_number;
				$result['vehicle_year_make_model'] = $row->vehicle_year_make_model;
				$result['vehicle_description'] = $row->vehicle_description;
				$result['vehicle_remarks'] = $row->vehicle_remarks;
				$result['vehicle_max_speed'] = $row->vehicle_max_speed;
				$result['vehicle_driver_name'] = $row->vehicle_driver_name;
				$result['vehicle_fathers_name'] = $row->vehicle_fathers_name;
				$result['vehicle_driver_license'] = $row->vehicle_driver_license;
				$result['vehicle_address'] = $row->vehicle_address;
				$result['vehicle_driver_phone'] = $row->vehicle_driver_phone;
				$result['vehicle_driver_dob'] = $row->vehicle_driver_dob;
				$result['vehicle_driver_notes'] = $row->vehicle_driver_notes;
				$result['vehicle_inserted_by'] = $row->vehicle_inserted_by;
				$result['vehicle_insert_date_time'] = $row->vehicle_insert_date_time;
				$result['vehicle_updated_by'] = $row->vehicle_updated_by;
				$result['vehicle_update_date_time'] = $row->vehicle_update_date_time;
				$result['vehicle_activation_status'] = $row->vehicle_activation_status;
				$result['vehicle_activated_by'] = $row->vehicle_activated_by;
				$result['vehicle_activated_date_time'] = $row->vehicle_activated_date_time;
				$result['vehicle_deactivated_by'] = $row->vehicle_deactivated_by;
				$result['vehicle_deactivated_date_time'] = $row->vehicle_deactivated_date_time;
				$result['vehicle_distence_per_unit'] = $row->vehicle_distence_per_unit;
				$result['vehicle_status'] = $row->vehicle_status;
			}
			return $result;
		} else {
			return false;
		}
	}

	function getHourlyReport($device_id = "", $from = "", $to = "")
	{
		$this->db->select('address, distance');
		//$this->db->select_sum('distance');
		$this->db->where('device_id', $device_id);
		$this->db->where('server_time >=', date('Y-m-d H:i:s', strtotime($from)));
		$this->db->where('server_time <=', date('Y-m-d H:i:s', strtotime($to)));
		$query = $this->db->get('tta_position');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$returnResult['distance'] = $row->distance;
				$returnResult['address'] = $row->address;
			}
			return $returnResult;
		}
	}


	function getDeviceInfoByImei($device_imei = "")
	{
		if (isset($device_imei) && $device_imei != NULL) {
			$this->db->where('device_imei', $device_imei);
			$this->db->where('device_status', 1);
			$query = $this->db->get('tta_devices');
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$result['device_id'] = $row->device_id;
					$result['vehicle_id'] = $row->vehicle_id;
					$result['device_type'] = $row->device_type;
					$result['device_imei'] = $row->device_imei;
					$result['device_unique_id'] = $row->device_unique_id;
					$result['device_activation_status'] = $row->device_activation_status;
					$result['device_activated_by'] = $row->device_activated_by;
					$result['device_activated_date_time'] = $row->device_activated_date_time;
					$result['device_deactivated_by'] = $row->device_deactivated_by;
					$result['device_deactivated_date_time'] = $row->device_deactivated_date_time;
					$result['device_authorization_code'] = $row->device_authorization_code;
					$result['device_authorizad_by'] = $row->device_authorizad_by;
					$result['device_authorized_date_time'] = $row->device_authorized_date_time;
					$result['device_deauthorized_by'] = $row->device_deauthorized_by;
					$result['device_deauthorized_date_time'] = $row->device_deauthorized_date_time;
					$result['device_asigned_status'] = $row->device_asigned_status;
					$result['device_asigned_to'] = $row->device_asigned_to;
					$result['device_asigned_date'] = $row->device_asigned_date;
					$result['device_sim_number'] = $row->device_sim_number;
					$result['device_current_url'] = $row->device_current_url;
					$result['device_old_url'] = $row->device_old_url;
					$result['device_update_url'] = $row->device_update_url;
					$result['device_varified'] = $row->device_varified;
					$result['device_firmware'] = $row->device_firmware;
					$result['device_version'] = $row->device_version;
					$result['device_sleep_interval'] = $row->device_sleep_interval;
					$result['device_note'] = $row->device_note;
					$result['device_status'] = $row->device_status;
					$result['device_inserted_by'] = $row->device_inserted_by;
					$result['device_insert_date_time'] = $row->device_insert_date_time;
					$result['device_updated_by'] = $row->device_updated_by;
					$result['device_update_date_time'] = $row->device_update_date_time;
				}
				return $result;
			} else {
				return false;
			}
		}
	}


	function get24hourlocation($device_id = '', $date = "")
	{
		$starttime = "00:00:00";
		$endtime = "23:59:00";
		$this->db->where('device_id', $device_id);
		$this->db->where('server_time >=', date('Y-m-d H:i:s', strtotime($date . ' ' . $starttime)));
		$this->db->where('server_time <=', date('Y-m-d H:i:s', strtotime($date . ' ' . $endtime)));
		//$this->db->where('server_time >=',"INTERVAL 1 HOUR");
		$this->db->order_by('position_id', 'asc');
		$query = $this->db->get('tta_position');
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}





	function getCoordinatesDistanceByDateRange($from_date = "", $to_date = "")
	{
		$days = $this->dateDiff($from_date, $to_date);
		for ($x = 0; $x <= $days; $x++) {
			$dateOut[date('Y-m-d', strtotime(date('Y-m-' . $x)))] = $this - getCoordinatesDistanceByDay(date('Y-m-d', strtotime(date('Y-m-' . $x))));
		}

		return $dateOut;
	}


	function getDistance($latitudeFrom = '', $longitudeFrom = '', $latitudeTo = '', $longitudeTo = '', $unit = "")
	{
		//Calculate distance from latitude and longitude
		$theta = $longitudeFrom - $longitudeTo;
		$dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		if ($unit == "K") {
			return ($miles * 1.609344) . ' km';
		} else if ($unit == "N") {
			return ($miles * 0.8684) . ' nm';
		} else {
			return $miles . ' mi';
		}

		//Way to call - getDistance($addressFrom, $addressTo, "K");  or getDistance($addressFrom, $addressTo, "N"); for notical mile
	}


	function getCoordinatesDistanceByDay($date = "")
	{
	}

	function dateDiff($d1, $d2)
	{
		// Return the number of days between the two dates:
		return round(abs(strtotime($d1) - strtotime($d2)) / 86400);
	}
}
