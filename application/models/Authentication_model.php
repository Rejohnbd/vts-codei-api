<?php class Authentication_model extends CI_Model
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function authenticate()
	{
		$response = '';
		$user_login_email = $this->input->post('user_login_email');
		$user_password = $this->input->post('user_password');
		if (isset($user_login_email) && $user_login_email != NULL && isset($user_password) && $user_password != NULL) {
			$returnedUser = $this->checkIfValidUser($user_login_email, $user_password);
			if (isset($returnedUser) && $returnedUser != NULL) {
				$response = array(
					'user' => $returnedUser,
					'message' => 'Login successful.'
				);
			} else {
				$response = array(
					'user' => 'No user found!',
					'message' => 'Invalid email or password!'
				);
			}
		} else {
			$response = array(
				'user' => 'Invalid email or password',
				'message' => 'Invalid Request!'
			);
		}
		$test[] = $response;
		header('Content-Type: application/json');
		echo json_encode($test);
	}

	function checkIfValidUser($email = '', $password = '')
	{
		$this->db->from('tta_users');
		$this->db->join('tta_customers', 'tta_customers.customer_id = tta_users.customer_id', 'left');
		$this->db->where('tta_users.user_login_email', $email);
		$this->db->where('tta_users.user_password', hash('sha512', $password));
		$this->db->where('tta_users.user_status', 1);
		$query = $this->db->get();
		if ($query->num_rows() == 1) {
			foreach ($query->result() as $row) {
				$result['primary_user'] = 1;
				$result['user_id'] = $row->user_id;
				$result['customer_id'] = $row->customer_id;
				$result['suspended'] = $row->suspended;
				$result['customer_name'] = $row->customer_name;
				$result['customer_address'] = $row->customer_address;
				$result['user_name'] = $row->user_name;
			}

			return $result;
		} else {
			return false;
		}
	}

	function getLastCoordinate()
	{
		$response = '';
		$device_imei = $this->input->post('device_imei');
		if (isset($device_imei) && $device_imei != NULL) {
			$deviceInfo = $this->getDeviceInfoByImei($device_imei);
			$returnedResult = $this->getLastCoordinatesByDeviceId($deviceInfo['device_id']);
			if (isset($returnedResult) && $returnedResult != NULL) {
				$response['success'] = 1;
				foreach ($returnedResult as $Coordinance) {
					$response['lat'] = $Coordinance->lat;
					$response['lng'] = $Coordinance->lng;
					$response['speed'] = $Coordinance->speed;
				}
				$response['message'] = 'Last Coordinate.';
			} else {
				$response[] = array(
					'success' => 0,
					'vehicles' => NULL,
					'message' => 'Invalid IMEI ID!'
				);
			}
		} else {
			$response[] = array(
				'success' => 0,
				'vehicles' => NULL,
				'message' => 'Invalid Request!'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}


	function getCoordinaneByPositionID($position_id = "")
	{
		$this->db - where('position_id', $position_id);
		$query = $this->db->get('tta_position');
		if ($query->num_rows() > 0) {
		}
	}

	function getLastCoordinatesByDeviceId($device_id = "")
	{
		$this->db->where('device_id', $device_id);
		$this->db->where('device_status', 1); // Active device
		$query = $this->db->get('tta_devices');
		if ($query->num_rows() > 0) {
			$this->db->where('device_id', $device_id);
			$this->db->order_by("position_id", "desc");
			$this->db->limit(2);
			$query1 = $this->db->get('tta_position');
			if ($query1->num_rows() > 1) {
				$count = 1;
				foreach ($query1->result() as $row) {
					if ($count == 1) {
						$customOutput["position_id"] = $row->position_id;
						$customOutput["device_id"] = $row->device_id;
						$customOutput["device_imei"] = $row->device_imei;
						$customOutput["server_time"] = $row->server_time;
						$customOutput["device_time"] = $row->device_time;
						$customOutput["lat"] = $row->lat;
						$customOutput["lng"] = $row->lng;
						$customOutput["distance"] = $row->distance;
						$customOutput["speed"] = $row->speed;
						$customOutput["engine_status"] = $row->engine_status;
						$customOutput["course"] = $row->course;
						$customOutput["address"] = $row->address;
						$customOutput["attributes"] = $row->attributes;
						$customOutput['message'] = "Successful.";
					} else {
						$customOutput["last_lat"] = $row->lat;
						$customOutput["last_lng"] = $row->lng;
					}
					$count++;
				}

				$test[] = $customOutput;
				header('Content-Type: application/json');
				echo json_encode($test);
				//$response['message'] = "Device Information.";
				//



			} else {
				$response['message'] = "No Data Found!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			}
		} else {
			$response['message'] = "Invalid Device!";
			$test[] = $response;
			header('Content-Type: application/json');
			echo json_encode($test);
		}
	}

	function getLastCoordinatesByDeviceImei($device_imei = "")
	{
		$this->db->where('device_imei', $device_imei);
		$this->db->order_by("position_id", "desc");
		$this->db->limit(1);
		$query1 = $this->db->get('tta_position');
		if ($query1->num_rows() == 1) {
			$response = $query1->result();
			header('Content-Type: application/json');
			echo json_encode($response);
		}
	}


	function getLastCoordinatesByVehicleID($vehicle_id = "")
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$query = $this->db->get('tta_vehicles');
		if ($query->num_rows() >= 0) {
			foreach ($query->result() as $row) {
				$device_id = $row->device_id;
				$response['driver_name'] = $row->vehicle_driver_name;
				$response['driver_phone'] = $row->vehicle_driver_phone;
				$response['vehicle_type_id'] = $row->vehicle_type_id;
				$response['vehicle_number'] = $row->vehicle_number;
				$response['vehicle_year_make_model'] = $row->vehicle_year_make_model;
			}

			if (isset($device_id) && $device_id != NULL) {
				$this->db->where('device_id', $device_id);
				$deviceQuery = $this->db->get('tta_devices');
				if ($deviceQuery->num_rows() == 1) {
					foreach ($deviceQuery->result() as $dRow) {
						$device_imei = $dRow->device_imei;
						$device_sim_number = $dRow->device_sim_number;
					}

					$this->db->where('device_imei', $device_imei);
					$this->db->order_by("position_id", "desc");
					$this->db->limit(1);
					$posQuery = $this->db->get('tta_position');
					foreach ($posQuery->result() as $pqrow) {
						$response['device_imei'] = $pqrow->device_imei;
						$response['server_time'] = $pqrow->server_time;
						$response['lat'] = $pqrow->lat;
						$response['lng'] = $pqrow->lng;
						$response['speed'] = $pqrow->speed;
						$response['course'] = $pqrow->course;
						$response['address'] = $pqrow->address;
						$response['engine_status'] = $pqrow->engine_status;
					}

					$response['device_sim_number'] = $device_sim_number;
					//$response['sms_body'] = "KJFHSKDUFHDSF";
					header('Content-Type: application/json');
					$test[] = $response;
					echo json_encode($test);
				}
			} else {
				header('Content-Type: application/json');
				$response['message'] = "No device assigned!";
				$test[] = $response;
				echo json_encode($test);
			}
		}
	}

	function getLast3Coordinance($device_imei = '')
	{
		$this->db->where('device_imei', $device_imei);
		$query = $this->db->get('tta_devices');
		foreach ($query->result() as $row) {
			$device_id = $row->device_id;
		}

		$this->db->where('device_id', $device_id);
		$this->db->order_by("position_id", "desc");
		$this->db->limit(3);
		$query1 = $this->db->get('tta_position');
		if ($query1->num_rows() == 3) {
			$result = $query1->result();
			return $result;
		}
	}


	function getVehicles($customer_id = "")
	{
		$response['success'] = '';
		if (isset($customer_id) && $customer_id != NULL) {
			$returnedResult = $this->getAllVehiclesByCustomerId($customer_id);
			if (isset($returnedResult) && $returnedResult != NULL) {
				$response['success'] = 1;
				$response['totalVehicles'] = sizeof($returnedResult);
				$count = 1;
				foreach ($returnedResult as $vehicle) {
					$vehicleList[] = $this->getVehicleInfoByVehicleID($vehicle->vehicle_id);
					$count++;
				}
				$response['vehicles'] = $vehicleList;
				$response['message'] = 'All Customer List.';
			} else {
				$response['vehicles'] = array(
					'success' => 0,
					'vehicles' => 'No vehicles',
					'message' => 'Invalid Customer ID!'
				);
			}
		} else {
			$response[] = array(
				'success' => 0,
				'vehicles' => 'No vehicles',
				'message' => 'Invalid Request!'
			);
		}
		header('Content-Type: application/json');
		$test[] = $response;
		echo json_encode($test);
	}

	function getVehicleInfoByVehicleID($vehicle_id = '')
	{
		$this->db->where('vehicle_id', $vehicle_id);
		$query = $this->db->get('tta_vehicles');
		if ($query->num_rows() == 1) {
			foreach ($query->result() as $row) {
				// Driver Picture
				$device_id = $row->device_id;
				$result['vehicle_id'] = $row->vehicle_id;
				$result['vehicle_type'] = $row->vehicle_type_id;
				$result['vehicle_icon'] = "http://service.mobitrackbd.com/vehicleIcons/car.png";
				$result['vehicle_number'] = $row->vehicle_number;
				$result['vehicle_year_make_model'] = $row->vehicle_year_make_model;
				$result['vehicle_description'] = $row->vehicle_description;
				//$result['vehicle_max_speed'] = $row->vehicle_max_speed;
				$result['vehicle_driver_name'] = $row->vehicle_driver_name;
				$result['vehicle_driver_phone'] = $row->vehicle_driver_phone;

				// Engine Status on or off
				$this->db->where('device_id', $device_id);
				$deviceQuery = $this->db->get('tta_devices');
				if ($deviceQuery->num_rows() == 1) {
					foreach ($deviceQuery->result() as $dRow) {
						$device_imei = $dRow->device_imei;
					}
					$this->db->where('device_imei', $device_imei);
					$this->db->order_by('position_id', 'DESC');
					$this->db->limit(1);
					$posQuery = $this->db->get('tta_position');
					if ($posQuery->num_rows() == 1) {
						foreach ($posQuery->result() as $pRow) {
							$result['lat'] = $pRow->lat;
							$result['lng'] = $pRow->lng;
							$result['engine_status'] = $pRow->engine_status;
						}
					}
				}

				// Vehicle Icon.

			}
			return $result;
		} else {
			return false;
		}
	}

	function getAllVehiclesByCustomerId($customer_id = '')
	{
		$this->db->select('vehicle_id, device_id, vehicle_type_id, vehicle_number, vehicle_year_make_model, vehicle_driver_name, vehicle_driver_phone, vehicle_distence_per_unit');
		$this->db->where('customer_id', $customer_id);
		$this->db->where('vehicle_status', 1);
		$this->db->order_by('tta_vehicles.vehicle_year_make_model', 'asc');
		$query = $this->db->get('tta_vehicles');
		if ($query->num_rows() > 0) {
			//return $query->result();
			header('Content-Type: application/json');
			$response['totalVehicles'] = $query->num_rows();
			$response['vehicles'] = $query->result();
			$response['message'] = "Successful";
			$test[] = $response;
			echo json_encode($test);
		} else {
			header('Content-Type: application/json');
			$response['message'] = "Invalid Custoemr ID.";
			echo json_encode($response);
		}
	}

	/**
	 * Added by Rejohn
	 * for web api
	 */
	function web_authenticate()
	{
		$user_login_email = $this->input->post('user_login_email');
		$user_password = $this->input->post('user_password');

		$this->db->from('tta_users');
		$this->db->join('tta_customers', 'tta_customers.customer_id = tta_users.customer_id', 'left');
		$this->db->join('tta_user_types', 'tta_user_types.user_type_id = tta_users.user_type_id', 'left');
		$this->db->where('tta_users.user_login_email', $user_login_email);
		$this->db->where('tta_users.user_password', hash('sha512', $user_password));
		$this->db->where('tta_users.user_status', 1);
		$query_result = $this->db->get();
		$result = $query_result->row();
		return $result;
	}
}
