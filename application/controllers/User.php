<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 

	}

	public function index()
	{
		echo 'User';
		//return false;
	}


	public function changePassword()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			if (isset($data['user_id'])) {
				$_POST['user_id'] = $data['user_id'];
			}
			if (isset($data['login_email'])) {
				$_POST['login_email'] = $data['login_email'];
			}
			if (isset($data['current_password'])) {
				$_POST['current_password'] = $data['current_password'];
			}
			if (isset($data['new_password'])) {
				$_POST['new_password'] = $data['new_password'];
			}
			//print_r($data);

			//--------------------------------------------------------------------------------------------------------------

			// validation security 
			$this->form_validation->set_rules("user_id", "User ID", "required");
			$this->form_validation->set_rules("login_email", "Login Email", "required");
			$this->form_validation->set_rules("current_password", "Current Password", "required");
			$this->form_validation->set_rules("new_password", "New Password", "required");
			if ($this->form_validation->run() == FALSE) {
				$response['message'] = "Validation Failed!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			} else {
				$this->load->model('user_model');
				$this->user_model->changePassword();
			}
		}
	}

	public function getUserListByCustomerIdUserId()
	{
		/*
		http://service.mobitrackbd.com/authentication/getVehicles
		
		Final
		{"user_login_email":"mehedi247@hotmail.com","user_password":"sheops1"}
		*/

		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			// customer_id
			if (isset($data['customer_id'])) {
				$_POST['customer_id'] = $data['customer_id'];
			}
			if (isset($data['user_id'])) {
				$_POST['user_id'] = $data['user_id'];
			}

			//print_r($data);

			//--------------------------------------------------------------------------------------------------------------

			// validation security 
			$this->form_validation->set_rules("customer_id", "Customer ID", "required");
			$this->form_validation->set_rules("user_id", "User ID", "required");
			if ($this->form_validation->run() == FALSE) {
				// show error
				$response['message'] = "Invalid Customer ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			} else {
				$this->task_model->getUserListByCustomerIdUserId();
			}
		}
	}
}
