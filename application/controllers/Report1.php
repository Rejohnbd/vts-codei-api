<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 

	}

	public function index()
	{
		echo 'report';
		//return false;
	}


	public function getDailyAvarageReport()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			if (isset($data['device_id'])) {
				$_POST['device_id'] = $data['device_id'];
			}
			if (isset($data['date'])) {
				$_POST['date'] = $data['date'];
			}
			//print_r($data);

			//--------------------------------------------------------------------------------------------------------------

			// validation security 
			$this->form_validation->set_rules("device_id", "Device ID", "required");
			$this->form_validation->set_rules("date", "Date", "required");
			if ($this->form_validation->run() == FALSE) {
				// show error
				echo form_error("device_imei") . '
';
			} else {
				$this->report_model->get24hrsAvarageReport();
			}
		}
	}

	public function getDailySummeryReport()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			if (isset($data['device_id'])) {
				$_POST['device_id'] = $data['device_id'];
			}
			if (isset($data['date'])) {
				$_POST['date'] = $data['date'];
			}
			if (isset($data['vehicle_distence_per_unit'])) {
				$_POST['vehicle_distence_per_unit'] = $data['vehicle_distence_per_unit'];
			}
			//print_r($data);

			//--------------------------------------------------------------------------------------------------------------

			// validation security 
			$this->form_validation->set_rules("device_id", "Device ID", "required");
			$this->form_validation->set_rules("date", "Date", "required");
			$this->form_validation->set_rules("vehicle_distence_per_unit", "Mileage", "required");
			if ($this->form_validation->run() == FALSE) {
				// show error
				echo form_error("device_imei") . '
';
			} else {
				$this->report_model->get24hrsSummeryReport();
			}
		}
	}

	public function getMonthlyReport()
	{
		/*
		http://service.mobitrackbd.com/report/getMonthlyReport
		
		Final
		{"device_imei":"123456789", "from_date":"2018/12/12", "to_date":"2018/12/13"}
		*/

		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			// customer_id
			if (isset($data['device_imei'])) {
				$_POST['device_imei'] = $data['device_imei'];
			}
			if (isset($data['from_date'])) {
				$_POST['from_date'] = $data['from_date'];
			}
			if (isset($data['to_date'])) {
				$_POST['to_date'] = $data['to_date'];
			}
			print_r($data);

			//--------------------------------------------------------------------------------------------------------------

			// validation security 
			$this->form_validation->set_rules("device_imei", "Device IMEI", "required");
			$this->form_validation->set_rules("from_date", "From Date", "required");
			$this->form_validation->set_rules("to_date", "To Date", "required");
			if ($this->form_validation->run() == FALSE) {
				// show error
				echo form_error("device_imei") . '';
				echo form_error("from_date") . '';
				echo form_error("to_date") . '';
			} else {
				$this->report_model->getMonthlyReport();
			}
		}
	}
}
