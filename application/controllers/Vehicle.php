<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vehicle extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 
		$this->load->model('authentication_model');
		$this->load->model('vehicle_model');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, app-token");
	}

	public function index()
	{
		return false;
	}


	public function getAllVehiclesByCustomerID()
	{
		if ($this->input->get_request_header('app-token', TRUE) == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['customer_id'])) {
					$_POST['customer_id'] = $data['customer_id'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("customer_id", "Customer ID", "required");
				if ($this->form_validation->run() == FALSE) {
					// show error
					$response['message'] = "Invalid Customer ID!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->authentication_model->getAllVehiclesByCustomerId($data['customer_id']);
				}
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function getLastCoordinate()
	{
		if ($this->input->get_request_header('app-token', TRUE) == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['device_id'])) {
					$_POST['device_id'] = $data['device_id'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("device_id", "Device ID", "required");
				if ($this->form_validation->run() == FALSE) {
					$response['message'] = "Validation Failed!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->authentication_model->getLastCoordinatesByDeviceId($data['device_id']);
				}
			} else {
				$response['message'] = "Invalid Inputs!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
				return false;
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function getLastCoordinateDemoGet()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {
			$this->load->model('fake_model');
			/*
			http://service.mobitrackbd.com/authentication/getLastCoordinate

			Final
			{"vehicle_id":"1"}
			*/

			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['device_id'])) {
					$_POST['device_id'] = $data['device_id'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("device_id", "Device ID", "required");
				if ($this->form_validation->run() == FALSE) {
					$response['message'] = "Validation Failed!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->fake_model->getFakeData($data['device_id']);
				}
			} else {
				$response['message'] = "Invalid Inputs!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
				return false;
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	public function getLastCoordinateDemoPost()
	{
		$headers = apache_request_headers();
		if (isset($headers['app-token']) && $headers['app-token'] == $this->config->item('app_token')) {

			$this->load->model('fake_model');

			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// customer_id
				if (isset($data['device_id'])) {
					$_POST['device_id'] = $data['device_id'];
				}
				if (isset($data['device_imei'])) {
					$_POST['device_imei'] = $data['device_imei'];
				}
				if (isset($data['server_time'])) {
					$_POST['server_time'] = $data['server_time'];
				}
				if (isset($data['device_time'])) {
					$_POST['device_time'] = $data['device_time'];
				}
				if (isset($data['lat'])) {
					$_POST['lat'] = $data['lat'];
				}
				if (isset($data['lng'])) {
					$_POST['lng'] = $data['lng'];
				}
				if (isset($data['distance'])) {
					$_POST['distance'] = $data['distance'];
				}
				if (isset($data['speed'])) {
					$_POST['speed'] = $data['speed'];
				}
				if (isset($data['engine_status'])) {
					$_POST['engine_status'] = $data['engine_status'];
				}
				if (isset($data['course'])) {
					$_POST['course'] = $data['course'];
				}
				if (isset($data['address'])) {
					$_POST['address'] = $data['address'];
				}
				if (isset($data['attributes'])) {
					$_POST['attributes'] = $data['attributes'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("device_id", "Position ID", "required");
				$this->form_validation->set_rules("device_imei", "Position ID", "required");
				$this->form_validation->set_rules("server_time", "Position ID", "required");
				$this->form_validation->set_rules("device_time", "Position ID", "required");
				$this->form_validation->set_rules("lat", "Position ID", "required");
				$this->form_validation->set_rules("lng", "Position ID", "required");
				$this->form_validation->set_rules("distance", "Position ID", "required");
				$this->form_validation->set_rules("speed", "Position ID", "required");
				$this->form_validation->set_rules("engine_status", "Position ID", "required");
				$this->form_validation->set_rules("course", "Position ID", "required");
				$this->form_validation->set_rules("address", "Position ID", "required");
				$this->form_validation->set_rules("attributes", "Position ID", "required");
				if ($this->form_validation->run() == FALSE) {
					$response['message'] = "Validation Failed!";
					$test[] = $response;
					header('Content-Type: application/json');
					echo json_encode($test);
				} else {
					$this->fake_model->addFakeData();
				}
			} else {
				$response['message'] = "Invalid Inputs!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
				return false;
			}
		} else {
			$response = array(
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	/**
	 * Added by Rejohn
	 * for web api
	 */
	public function isJSON($string)
	{
		return is_string($string) && is_array(json_decode($string, true)) ? true : false;
	}

	public function getAllVehicleTypes()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$result = $this->vehicle_model->get_all_vehicle_types();
			$data = array(
				'status'			=> 200,
				'message'			=> "All Vehicle Types",
				'vehicle_types'		=> $result
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	public function getAllVehicleColors()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$result = $this->vehicle_model->get_all_vehicle_colors();
			$data = array(
				'status'			=> 200,
				'message'			=> "All Vehicle Colors",
				'vehicle_colors'	=> $result
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	public function addVehicleToCustomer()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$req_data = file_get_contents('php://input');
			if ($this->isJSON($req_data)) {
				$data = json_decode($req_data, true);

				$_POST['customer_id']      					= $data['customer_id'];
				$_POST['vehicle_number']  					= $data['vehicle_number'];
				$_POST['vehicle_max_speed']    				= $data['vehicle_max_speed'];
				$_POST['vehicle_type_id']					= $data['vehicle_type_id'];
				$_POST['vehicle_color_id']    				= $data['vehicle_color_id'];
				$_POST['vehicle_year_make_model']    		= $data['vehicle_year_make_model'];
				$_POST['vehicle_description']    			= $data['vehicle_description'];
				$_POST['vehicle_device_cost']    			= $data['vehicle_device_cost'];
				$_POST['vehicle_monthly_recurring_charge']	= $data['vehicle_monthly_recurring_charge'];
				$_POST['vehicle_distence_per_unit']    		= $data['vehicle_distence_per_unit'];
				$_POST['vehicle_driver_name']    			= $data['vehicle_driver_name'];
				$_POST['vehicle_fathers_name']    			= $data['vehicle_fathers_name'];
				$_POST['vehicle_driver_phone']    			= $data['vehicle_driver_phone'];
				$_POST['vehicle_driver_dob']    			= $data['vehicle_driver_dob'];
				$_POST['vehicle_driver_license']    		= $data['vehicle_driver_license'];
				$_POST['vehicle_address']    				= $data['vehicle_address'];
				$_POST['vehicle_inserted_by']    				= $data['vehicle_inserted_by'];

				$this->form_validation->set_rules('customer_id', "Customer Id", "required|integer");
				$this->form_validation->set_rules('vehicle_number', "Vehicle Number", "required");
				$this->form_validation->set_rules('vehicle_type_id', "Vehicle Type ID", "required|integer");
				$this->form_validation->set_rules('vehicle_color_id', "Vehicle Color ID", "required|integer");
				$this->form_validation->set_rules('vehicle_year_make_model', "Vehicle Model & Year", "required");
				$this->form_validation->set_rules('vehicle_device_cost', "Vehicle Device Cost", "required|integer");
				$this->form_validation->set_rules('vehicle_monthly_recurring_charge', "Vehicle Monthly Charge", "required|integer");
				$this->form_validation->set_rules('vehicle_driver_name', "Vehicle Monthly Charge", "required");
				$this->form_validation->set_rules('vehicle_driver_phone', "Driver Phone Number", "required|integer|min_length[11]|max_length[11]");
				$this->form_validation->set_rules('vehicle_inserted_by', "Vehicle Inserted By", "required|integer");

				if ($this->form_validation->run() == FALSE) {
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_id'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('vehicle_number'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_sim_number'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('vehicle_color_id'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('vehicle_year_make_model'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('vehicle_device_cost'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('vehicle_monthly_recurring_charge'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('vehicle_driver_name'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('vehicle_driver_phone'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('vehicle_inserted_by'));

					$data = array(
						'status'    => 406,
						'message'    => "Not Acceptable",
						"errors"    => array_values(array_filter($errors))
					);
					header('Content-Type: application/json');
					echo json_encode($data);
				} else {
					$this->vehicle_model->add_vehicle_to_customer($_POST);
					$data = array(
						'message'	=> "Vehicle Create Successfully.",
						'status'	=> 201
					);
					header('Content-Type: application/json');
					echo json_encode($data);
				}
			} else {
				$data = array(
					'message'    => "Bad Request",
					'status'    => 400
				);
				header('Content-Type: application/json');
				echo json_encode($data);
			}
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}
}
