<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 
		$this->load->model('authentication_model');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, app-token");
	}

	public function index()
	{
		//return false;
	}

	public function authenticate()
	{
		if ($this->input->get_request_header('app-token', TRUE) == $this->config->item('app_token')) {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				// imei
				if (isset($data['user_login_email'])) {
					$_POST['user_login_email'] = $data['user_login_email'];
				}
				// Lat
				if (isset($data['user_password'])) {
					$_POST['user_password'] = $data['user_password'];
				}

				//print_r($data);

				//--------------------------------------------------------------------------------------------------------------

				// validation security 
				$this->form_validation->set_rules("user_login_email", "Email", "valid_email|required");
				$this->form_validation->set_rules("user_password", "Pass", "trim|required");
				if ($this->form_validation->run() == FALSE) {
					header('Content-Type: application/json');
					$response['message'] = "Validation failed!";
					$test[] = $response;
					echo json_encode($test);
				} else {
					$this->authentication_model->authenticate();
				}
			} else {
				header('Content-Type: application/json');
				$response['message'] = "Invalid Inputs!";
				$test[] = $response;
				echo json_encode($test);
				return false;
			}
		} else {
			$response = array(
				'user' => 'No user found!',
				'message' => 'Invalid token'
			);

			header('Content-Type: application/json');
			echo json_encode($response);
			return false;
		}
	}

	/**
	 * Added by Rejohn
	 * for web api
	 */

	public function webAuthentication()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$data = json_decode(file_get_contents('php://input'), true);
			if (isset($data) && $data != NULL) {
				if (isset($data['email']) && isset($data['password'])) {
					$_POST['user_login_email']	= $data['email'];
					$_POST['user_password'] 	= $data['password'];
					// 
					$this->form_validation->set_rules("user_login_email", "Email", "valid_email|required");
					$this->form_validation->set_rules("user_password", "Pass", "trim|required");
					if ($this->form_validation->run() == FALSE) {
						$data = array(
							'message'	=> "No Accepted",
							'status'	=> 406
						);
						header('Content-Type: application/json');
						echo json_encode($data);
					} else {
						$result = $this->authentication_model->web_authenticate();
						if ($result) {
							$user['user_id'] 		= $result->user_id;
							$user['customer_id'] 	= $result->customer_id;
							$user['suspended'] 		= $result->suspended;
							$user['user_name'] 		= $result->user_name;
							$user['user_address']	= $result->customer_address;
							$user['user_type'] 		= $result->type_name;
							$user['user_email'] 	= $result->user_login_email;

							$data = array(
								'users'		=> $user,
								'status'	=> 200,
								'message'	=> "Login successful."
							);
							header('Content-Type: application/json');
							echo json_encode($data);
						} else {
							$data = array(
								'message'	=> "No Content",
								'status'	=> 204
							);
							header('Content-Type: application/json');
							echo json_encode($data);
						}
					}
				} else {
					$data = array(
						'message'	=> "Non-authoritative Information",
						'status'	=> 203
					);
					header('Content-Type: application/json');
					echo json_encode($data);
				}
			} else {
				$data = array(
					'message'	=> "No Content",
					'status'	=> 204
				);
				header('Content-Type: application/json');
				echo json_encode($data);
			}
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}
}
