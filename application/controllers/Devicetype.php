<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Devicetype extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
        //date_default_timezone_set($this->config->item('time_zone')); 
        $this->load->model('device_type_model');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, app-token");
    }

    public function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }

    public function getAllDeviceTypes()
    {
        if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
            $result = $this->device_type_model->get_all_device_types();

            $data = array(
                'device_types'  => $result,
                'status'        => 200,
                'message'       => "All Device Types"
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        } else {
            $data = array(
                'message'    => "Bad Request",
                'status'    => 400
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    public function createDeviceTypes()
    {
        if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
            $req_data = file_get_contents('php://input');
            if ($this->isJSON($req_data)) {
                $data = json_decode($req_data, true);

                $_POST['device_type_name']      = $data['device_type_name'];
                $_POST['device_type_status']    = $data['device_type_status'];

                $this->form_validation->set_rules('device_type_name', "Device Type Name", "required|is_unique[tta_device_type.device_type_name]");
                $this->form_validation->set_rules('device_type_status', "Device Type Status", "required|integer|min_length[1]|max_length[1]");

                if ($this->form_validation->run() == FALSE) {
                    $errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_type_name'));
                    $errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_type_status'));

                    $data = array(
                        'status'    => 406,
                        'message'    => "Not Acceptable",
                        "errors"    => array_values(array_filter($errors))
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {
                    $this->device_type_model->create_device_type($_POST);

                    $data = array(
                        'message'   => "Device Type Created Successfully.",
                        'status'    => 201
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                }
            } else {
                $data = array(
                    'message'    => "Bad Request",
                    'status'    => 400
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        } else {
            $data = array(
                'message'    => "Bad Request",
                'status'    => 400
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    public function getDeviceTypeById()
    {
        if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
            $req_data = file_get_contents('php://input');
            if ($this->isJSON($req_data)) {
                $data = json_decode($req_data, true);

                $_POST['device_type']  = $data['device_type'];
                $this->form_validation->set_rules('device_type', "Device Type Status", "required|integer");

                if ($this->form_validation->run() == FALSE) {
                    $errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_type'));

                    $data = array(
                        'status'    => 406,
                        'message'    => "Not Acceptable",
                        "errors"    => array_values(array_filter($errors))
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {
                    $checkExistance = $this->device_type_model->check_device_type_exist($_POST['device_type']);

                    if ($checkExistance) {
                        $result = $this->device_type_model->get_device_type_by_id($_POST['device_type']);

                        $data = array(
                            'device_type'   => $result,
                            'message'       => "Device Type Information.",
                            'status'        => 201
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    } else {
                        $data = array(
                            'status'    => 204,
                            'message'   => "Device Type Not Found.",
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }
                }
            } else {
                $data = array(
                    'message'    => "Bad Request",
                    'status'    => 400
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        } else {
            $data = array(
                'message'    => "Bad Request",
                'status'    => 400
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    public function updateDeviceTypeById()
    {
        if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
            $req_data = file_get_contents('php://input');
            if ($this->isJSON($req_data)) {
                $data = json_decode($req_data, true);

                $_POST['device_type']           = $data['device_type'];
                $_POST['device_type_name']      = $data['device_type_name'];
                $_POST['device_type_status']    = $data['device_type_status'];

                $this->form_validation->set_rules('device_type', "Device Type", "required|integer");
                $this->form_validation->set_rules('device_type_name', "Device Type Name", "required");
                $this->form_validation->set_rules('device_type_status', "Device Type Status", "required|integer|min_length[1]|max_length[1]");

                if ($this->form_validation->run() == FALSE) {
                    $errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_type'));
                    $errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_type_name'));
                    $errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_type_status'));

                    $data = array(
                        'status'    => 406,
                        'message'    => "Not Acceptable",
                        "errors"    => array_values(array_filter($errors))
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {

                    $checkExistance = $this->device_type_model->check_device_type_exist($_POST['device_type']);

                    if ($checkExistance) {
                        $checkExistance = $this->device_type_model->check_device_type_exist_by_name($_POST['device_type_name']);

                        if (!$checkExistance) {
                            $result = $this->device_type_model->update_device_type_by_id($_POST);

                            $data = array(
                                'device_type'   => $result,
                                'message'       => "Device Type Information.",
                                'status'        => 201
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                        } else {
                            $data = array(
                                'status'    => 409,
                                'message'   => "Device Type Name Already Exist.",
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                        }
                    } else {
                        $data = array(
                            'status'    => 204,
                            'message'   => "Device Type Not Found.",
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }
                }
            } else {
                $data = array(
                    'message'    => "Bad Request",
                    'status'    => 400
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        } else {
            $data = array(
                'message'    => "Bad Request",
                'status'    => 400
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }
}
