<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 
		$this->load->model('customer_model');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, app-token");
	}

	public function index()
	{
		return false;
	}


	public function getProfile()
	{
		/*
		http://service.mobitrackbd.com/authentication/getVehicles
		
		Final
		{"user_login_email":"mehedi247@hotmail.com","user_password":"sheops1"}
		*/

		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			// customer_id
			if (isset($data['customer_id'])) {
				$_POST['customer_id'] = $data['customer_id'];
			}
			if (isset($data['user_id'])) {
				$_POST['user_id'] = $data['user_id'];
			}

			//print_r($data);

			//--------------------------------------------------------------------------------------------------------------

			// validation security 
			$this->form_validation->set_rules("customer_id", "Customer ID", "required");
			$this->form_validation->set_rules("user_id", "User ID", "required");
			if ($this->form_validation->run() == FALSE) {
				// show error
				$response['message'] = "Invalid Customer ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			} else {
				$this->customer_model->getProfile($data['customer_id'], $data['user_id']);
			}
		}
	}

	public function updateProfile()
	{
		/*
		http://service.mobitrackbd.com/authentication/getVehicles
		
		Final
		{"user_login_email":"mehedi247@hotmail.com","user_password":"sheops1"}
		*/

		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			// customer_id
			if (isset($data['customer_id'])) {
				$_POST['customer_id'] = $data['customer_id'];
			}
			if (isset($data['user_id'])) {
				$_POST['user_id'] = $data['user_id'];
			}
			if (isset($data['vehicle_id'])) {
				$_POST['vehicle_id'] = $data['vehicle_id'];
			}
			if (isset($data['vehicle_max_speed'])) {
				$_POST['vehicle_max_speed'] = $data['vehicle_max_speed'];
			}
			if (isset($data['vehicle_driver_name'])) {
				$_POST['vehicle_driver_name'] = $data['vehicle_driver_name'];
			}
			if (isset($data['vehicle_driver_license'])) {
				$_POST['vehicle_driver_license'] = $data['vehicle_driver_license'];
			}
			if (isset($data['vehicle_address'])) {
				$_POST['vehicle_address'] = $data['vehicle_address'];
			}
			if (isset($data['vehicle_driver_phone'])) {
				$_POST['vehicle_driver_phone'] = $data['vehicle_driver_phone'];
			}
			if (isset($data['vehicle_distence_per_unit'])) {
				$_POST['vehicle_distence_per_unit'] = $data['vehicle_distence_per_unit'];
			}
			//print_r($data);

			// validation security 
			$this->form_validation->set_rules("customer_id", "Customer ID", "required");
			$this->form_validation->set_rules("user_id", "User ID", "required");
			$this->form_validation->set_rules("vehicle_id", "Vehicle ID", "required");
			$this->form_validation->set_rules("vehicle_max_speed", "Speed ID", "trim");
			$this->form_validation->set_rules("vehicle_driver_name", "Driver Name", "trim");
			$this->form_validation->set_rules("vehicle_driver_license", "License", "trim");
			$this->form_validation->set_rules("vehicle_address", "Address", "trim");
			$this->form_validation->set_rules("vehicle_driver_phone", "Phone", "trim");
			$this->form_validation->set_rules("vehicle_distence_per_unit", "Kilometer Per Liter", "trim");
			if ($this->form_validation->run() == FALSE) {
				// show error
				$response['message'] = "Invalid Customer ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			} else {
				$this->customer_model->updateProfile($data['customer_id'], $data['user_id'],  $data['vehicle_id']);
			}
		}
	}

	/**
	 * Added by Rejohn
	 * for web api
	 */
	public function isJSON($string)
	{
		return is_string($string) && is_array(json_decode($string, true)) ? true : false;
	}

	public function createCustomer()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$req_data = file_get_contents('php://input');
			if ($this->isJSON($req_data)) {
				$data = json_decode($req_data, true);

				$_POST['customer_type_id'] 			= $data['customer_type_id'];
				$_POST['customer_name'] 			= $data['customer_name'];
				$_POST['customer_primary_email'] 	= $data['customer_primary_email'];
				$_POST['customer_billing_email'] 	= $data['customer_billing_email'];
				$_POST['customer_alert_email'] 		= $data['customer_alert_email'];
				$_POST['customer_primary_phone'] 	= $data['customer_primary_phone'];
				$_POST['customer_secondary_phone']	= $data['customer_secondary_phone'];
				$_POST['customer_address'] 			= $data['customer_address'];
				$_POST['customer_billing_date'] 	= $data['customer_billing_date'];
				$_POST['customer_cost_per_device'] 	= $data['customer_cost_per_device'];
				$_POST['customer_cost_per_vehicle']	= $data['customer_cost_per_vehicle'];
				$_POST['user_password'] 			= $data['user_password'];
				$_POST['customer_inserted_by'] 		= $data['customer_inserted_by'];
				$_POST['customer_activated_by'] 	= $data['customer_activated_by'];
				$_POST['customer_notes'] 			= $data['customer_notes'];
				$_POST['customer_remarks'] 			= $data['customer_remarks'];
				$_POST['customer_remarks'] 			= $data['customer_remarks'];
				$_POST['customer_sms'] 				= $data['customer_sms'];
				$_POST['customer_rederred_by'] 		= $data['customer_rederred_by'];

				$this->form_validation->set_rules('customer_type_id', "Customer Type ID", "required|integer|min_length[1]|max_length[1]");
				$this->form_validation->set_rules('customer_name', "Customer Name", "required");
				$this->form_validation->set_rules('customer_primary_email', "Customer Primary Email", "required|valid_email");
				$this->form_validation->set_rules('customer_billing_email', "Customer Billing Email", "required|valid_email");
				$this->form_validation->set_rules('customer_alert_email', "Customer Alert Email", "required|valid_email");
				$this->form_validation->set_rules('customer_primary_phone', "Customer Primary Phone", "required|integer|min_length[11]|max_length[11]");
				$this->form_validation->set_rules('customer_secondary_phone', "Customer Secondary Phone", "required|integer|min_length[11]|max_length[11]");
				$this->form_validation->set_rules('customer_address', "Customer Address", "required");
				$this->form_validation->set_rules('customer_billing_date', "Customer Billing Date", "required|numeric");
				$this->form_validation->set_rules('customer_cost_per_device', "Cost Per Device", "required|numeric");
				$this->form_validation->set_rules('customer_cost_per_vehicle', "Cost Per Vehicle", "required|numeric");
				$this->form_validation->set_rules('user_password', "Customer Password", "required|min_length[8]");
				$this->form_validation->set_rules('customer_inserted_by', "Customer Inserted By", "required|numeric");
				$this->form_validation->set_rules('customer_activated_by', "Customer Actived By", "required|numeric");

				if ($this->form_validation->run() == FALSE) {
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_type_id'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_name'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_primary_email'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_billing_email'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_alert_email'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_primary_phone'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_secondary_phone'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_address'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_billing_date'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_cost_per_device'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_cost_per_vehicle'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('user_password'));

					$data = array(
						'status'	=> 406,
						'message'	=> "Not Acceptable",
						"errors"	=> array_values(array_filter($errors))
					);
					header('Content-Type: application/json');
					echo json_encode($data);
				} else {
					$this->customer_model->creae_customer($_POST);

					$data = array(
						'message'	=> "Customer Create Successfully.",
						'status'	=> 201
					);
					header('Content-Type: application/json');
					echo json_encode($data);
				}
			} else {
				$data = array(
					'message'	=> "Bad Request",
					'status'	=> 400
				);
				header('Content-Type: application/json');
				echo json_encode($data);
			}
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	public function numberOfAllCustomers()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$result = $this->customer_model->number_of_all_customers();
			$data = array(
				'total_customer'	=> $result,
				'status'			=> 200,
				'message'			=> "Number of Total Customer"
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	public function numberOfActiveCustomers()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$result = $this->customer_model->number_of_active_customers();
			$data = array(
				'active_customers'	=> $result,
				'status'			=> 200,
				'message'			=> "Number of Active Customer"
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	public function allCustomers()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$result = $this->customer_model->all_customers_with_vehicle();
			$data = array(
				'status'	=> 200,
				'message'	=> "Customer list with number of uses vehicle",
				'customers'	=> $result,
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	public function customerDetailsById()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$req_data = file_get_contents('php://input');
			if ($this->isJSON($req_data)) {
				$data = json_decode($req_data, true);

				$_POST['customer_id'] = $data['customer_id'];
				$this->form_validation->set_rules('customer_id', "Customer ID", "required|integer");

				if ($this->form_validation->run() == FALSE) {
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('customer_id'));

					$data = array(
						'status'	=> 406,
						'message'	=> "Not Acceptable",
						"errors"	=> array_values(array_filter($errors))
					);
					header('Content-Type: application/json');
					echo json_encode($data);
				} else {
					$customer_details = $this->customer_model->get_customer_details_by_id($_POST['customer_id']);
					$user_info = $this->customer_model->get_user_info_by_customer_id($_POST['customer_id']);
					$vehicle_infos = $this->customer_model->get_vehicle_info_by_customer_id($_POST['customer_id']);

					$data = array(
						'status'			=> 200,
						'message'			=> "Customer Details with User info, vechile info",
						'customer_details'	=> $customer_details,
						'user_info'			=> $user_info,
						'vehicle_infos'		=> $vehicle_infos

					);
					header('Content-Type: application/json');
					echo json_encode($data);
				}
			} else {
				$data = array(
					'message'	=> "Bad Request",
					'status'	=> 400
				);
				header('Content-Type: application/json');
				echo json_encode($data);
			}
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}
}
