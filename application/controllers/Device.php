<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Device extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '-1');
		//date_default_timezone_set($this->config->item('time_zone')); 
		$this->load->model('device_model');
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, app-token");
	}

	public function index()
	{
		return false;
	}


	public function inquiryDataByDeviceId()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			// device_id
			if (isset($data['device_id'])) {
				$_POST['device_id'] = $data['device_id'];
			}

			// validation security 
			$this->form_validation->set_rules("device_id", "Device ID", "required");
			if ($this->form_validation->run() == FALSE) {
				// show error
				$response['message'] = "Invalid Device ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			} else {
				$this->device_model->inquiryDataByDeviceId($data['device_id']);
			}
		}
	}

	public function downloadDataByDeviceId()
	{
		$data = json_decode(file_get_contents('php://input'), true);
		if (isset($data) && $data != NULL) {
			// device_id
			if (isset($data['device_id'])) {
				$_POST['device_id'] = $data['device_id'];
			}

			// validation security 
			$this->form_validation->set_rules("device_id", "Device ID", "required");
			if ($this->form_validation->run() == FALSE) {
				// show error
				$response['message'] = "Invalid Device ID!";
				$test[] = $response;
				header('Content-Type: application/json');
				echo json_encode($test);
			} else {
				$this->device_model->downloadDataByDeviceId();
			}
		}
	}

	/**
	 * Added by Rejohn
	 * for web api
	 */
	public function isJSON($string)
	{
		return is_string($string) && is_array(json_decode($string, true)) ? true : false;
	}

	public function numberOfAllDevices()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$result = $this->device_model->number_of_all_devicess();
			$data = array(
				'total_device'	=> $result,
				'status'			=> 200,
				'message'			=> "Number of Total Device"
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	public function numberOfActiveDevices()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$result = $this->device_model->number_of_active_devices();
			$data = array(
				'active_device'		=> $result,
				'status'			=> 200,
				'message'			=> "Number of Active Device"
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			$data = array(
				'message'	=> "Bad Request",
				'status'	=> 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}



	public function addDevice()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$req_data = file_get_contents('php://input');
			if ($this->isJSON($req_data)) {
				$data = json_decode($req_data, true);

				$_POST['device_type']      			= $data['device_type'];
				$_POST['device_unique_id']  		= $data['device_unique_id'];
				$_POST['device_imei']    			= $data['device_imei'];
				$_POST['device_sim_number']			= $data['device_sim_number'];
				$_POST['device_note']    			= $data['device_note'];
				$_POST['device_sim_type']   		= $data['device_sim_type'];
				$_POST['device_asigned_status']   	= $data['device_asigned_status'];
				$_POST['device_recharge_amount']	= $data['device_recharge_amount'];
				$_POST['device_inserted_by']   		= $data['device_inserted_by'];

				$this->form_validation->set_rules('device_type', "Device Type", "required|integer");
				$this->form_validation->set_rules('device_unique_id', "Device Unique Id", "required|integer|is_unique[tta_devices.device_unique_id]");
				$this->form_validation->set_rules('device_imei', "Device IMEI", "required|integer|is_unique[tta_devices.device_imei]");
				$this->form_validation->set_rules('device_sim_number', "Device Sim Number", "required|integer|min_length[11]|max_length[11]");
				$this->form_validation->set_rules('device_sim_type', "Device Sim Type", "required|integer|min_length[1]|max_length[1]");
				$this->form_validation->set_rules('device_asigned_status', "Device Assign Status", "required|integer|min_length[1]|max_length[1]");

				if ($this->form_validation->run() == FALSE) {
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_type'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_unique_id'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_imei'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_sim_number'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_sim_type'));
					$errors[] = preg_replace('#^<p>|</p>$#i', '', form_error('device_asigned_status'));

					$data = array(
						'status'    => 406,
						'message'    => "Not Acceptable",
						"errors"    => array_values(array_filter($errors))
					);
					header('Content-Type: application/json');
					echo json_encode($data);
				} else {
					if ($_POST['device_unique_id'] != $_POST['device_imei']) {
						$data = array(
							'status'    => 406,
							'message'   => "Device Unique Id and Device Imei is not same."
						);
						header('Content-Type: application/json');
						echo json_encode($data);
					} else {
						$this->device_model->add_device($_POST);
						$data = array(
							'message'	=> "Device Create Successfully.",
							'status'	=> 201
						);
						header('Content-Type: application/json');
						echo json_encode($data);
					}
				}
			} else {
				$data = array(
					'message'    => "Bad Request",
					'status'    => 400
				);
				header('Content-Type: application/json');
				echo json_encode($data);
			}
		} else {
			$data = array(
				'message'    => "Bad Request",
				'status'    => 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}

	public function getAllDevices()
	{
		if ($this->input->get_request_header('app-token', TRUE) === $this->config->item('app_token') && $this->input->method(TRUE) === "POST") {
			$result = $this->device_model->get_all_devices();
			$data = array(
				'status'	=> 200,
				'message'	=> "All Devices",
				'customers'	=> $result,
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		} else {
			$data = array(
				'message'    => "Bad Request",
				'status'    => 400
			);
			header('Content-Type: application/json');
			echo json_encode($data);
		}
	}
}
